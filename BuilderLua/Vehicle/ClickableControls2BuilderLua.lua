ClickableControls2BuilderLua = {}
-- By RhoSigma

ClickableControls2BuilderLua.version = "1.1 20/06/2020"
ClickableControls2BuilderLua.enabled = false
ClickableControls2BuilderLua.cc2 = ClickableControls2

ClickableControls2BuilderLua.ToolButton = {
  name                  = "Clickable Controls 2",
  icon                  = HBU.GetBuilderLuaFolder() .. "/Vehicle/ClickableControls2BuilderLua/Assets/icon.png",
  tooltip               = "Clickable Controls 2 Editor: Edit clickables, including scale, range, and animation.",
  bigTooltip            = [[Clickable Controls 2 Editor:
Edit selected clickables
Displays selected clickable hitbox]],
  hoversound            = "click_blip",
  clicksound            = "affirm_blip_1",
  imageLayout           = {"dualcolor",true},
  --layout                = {"shortcut",{func = function() self:OnSelected(); Builder.SelectTool(ClickableControls2BuilderLua) end}},
  shortcut              = nil,
}

function ClickableControls2BuilderLua:Start()
	if Builder and Builder.RegisterTool then Builder.RegisterTool(self,self.ToolButton) end
	HBBuilder.Builder.RegisterCallback(HBBuilder.BuilderCallback.InspectorTargetChanged,"ClickableControls2BuilderLua onInspectorTargetChanged",function() self:OnInspectorChanged() end)
	HBBuilder.Builder.RegisterCallback(HBBuilder.BuilderCallback.PreSaveProject,"ClickableControls2BuilderLua presave cleanup",function() self:OnPreSave() end)


	-- disable CC2 main update
	self.cc2.enabled = false

	-- build all clickable display names into reverse lookup table, mostly to handle the type enum
	self.clickableTypeReverseLookup = {}
	for key, cType in pairs(ClickableControls2.Clickables.clickableTypes) do
		if cType.displayName then
			self.clickableTypeReverseLookup[cType.displayName] = key
		else
			self.clickableTypeReverseLookup[key] = key -- simplest way to handle clickable types without a display name
		end
	end
end

function ClickableControls2BuilderLua:OnDestroy()
	if Builder and Builder.UnRegisterTool then Builder.UnRegisterTool(self,self.ToolButton) end
	-- re-enable CC2 main update
	self.cc2.enabled = true
end

-- presave cleanup, fix, and optimisation pass
function ClickableControls2BuilderLua:OnPreSave()
	self.VMS.Cleanup() -- remove temporary VMS registrations

	-- check for networking components, add if necessary
	for button in Slua.iter(HBBuilder.Builder.root:GetComponentsInChildren("HBButton")) do
		local sDataComp = button.gameObject:GetComponent("HBBuilder.StringDataContainer")
		if sDataComp then
			if HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, "clickableControl") == "true" then
				-- check for networking components, add if necessary
				if not button.gameObject:GetComponent("HBNetworking.NetworkBase") then
					button.gameObject:AddComponent("HBNetworking.NetworkBase")
				end
				if not button.gameObject:GetComponent("HBNetworking.General") then
					button.gameObject:AddComponent("HBNetworking.General")
				end

				-- check for empty tag components, cleanup nil tags and remove empty tag containers

				if button.gameObject.transform:Find("StateTags") then
					local cleanup = false
					local sDataComp = button.gameObject.transform:Find("StateTags").gameObject:GetComponent("HBBuilder.StringDataContainer")
					Debug.Log(sDataComp.stringData)
					if sDataComp then

						local tags = {}
						local tagIndex = 1
						for key in Slua.iter(HBBuilder.BuilderUtils.GetStringDataKeys(sDataComp.stringData)) do
							local tag = tostring(HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, key))
							Debug.Log(tag)
							if tag ~= "" then
								tags[#tags+1] = {key, tag}
							end
						end

						local tagIndex = 1
						local stringData = ""
						for k, tag in pairs(tags) do
							stringData = self.StringData.Set(stringData, "stateTag" .. tostring(tagIndex), tostring(tag[2]))
							tagIndex = tagIndex + 1
						end
						sDataComp.stringData = stringData

						if sDataComp.stringData == "" then
							cleanup = true
						end
					else
						cleanup = true
					end

					if cleanup then
						GameObject.DestroyImmediate(button.gameObject.transform:Find("StateTags").gameObject)
					end
				end
			end
		end
	end


end

function ClickableControls2BuilderLua:OnInspectorChanged()
	if self and not self.enabled then return end

	self.clickables = {} -- init this here so that it gets cleared properly

	if #iter(Inspector.lastTarget) < 1 then return end -- nothing selected

	-- check if this is a clickable, add to list if so
	-- TODO: switch to GetComponentsInChildrenLimited
	local buttons = {}
	local selection = iter(Inspector.lastTarget)
	for i=1, #selection do
		local object = selection[i]
		for i=0,object.transform.childCount-1 do
			if object.transform:GetChild(i) then
				local button = object.transform:GetChild(i):GetComponent("HBButton")
				if button then
					local sDataComp = button.gameObject:GetComponent("HBBuilder.StringDataContainer")
					if sDataComp then
						if HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, "clickableControl") == "true" then
							table.insert(buttons, button)
						end
					end
				end
			end
		end
	end

	-- seat listing
	local seats = {}
	for i=1, #selection do
		local object = selection[i]
		for i=0,object.transform.childCount-1 do
			if object.transform:GetChild(i) then
				local seat = object.transform:GetChild(i):GetComponent("HBSeat")
				if seat then
					table.insert(seats, seat)
				end
			end
		end
	end

	if #buttons < 1 and #seats < 1 then return end -- bail if no buttons found
	for i=1,#buttons do
		local clickable = buttons[i]
		local clickableData = self:BuildClickableData(clickable)
		if clickableData then
			table.insert(self.clickables, clickableData)
		end
	end
	if #self.clickables < 1 and #seats < 1 then return end -- bail if no clickables found

	self.wizzardConfig = self:BuildWizardData(self.clickables, seats)
	--handle wizard UI
	Inspector:CreateProperties(self.wizzardConfig)
end

function ClickableControls2BuilderLua:Update()
	if not self.enabled then
		if self.wasEnabled then
			-- frame after being disabled
			self.VMS.Cleanup()
			self.wasEnabled = false
		end
		self.cc2.Interaction.ToggleClickMode(false)
		return
	end

	if not self.wasEnabled then
		-- frame after being enabled
		self.VMS.PseudoInit()
		self.wasEnabled = true

		-- lazy notification to prompt for save, fix later
		HBBuilder.Builder.ChangedCurrentAssembly()
	end

    ------------------------------------------------------------------
    -- valid state checking and auto chat open/click mode returning --
    ------------------------------------------------------------------

    -- check if we should open chat this frame, workaround for issues arising from trying to open chat same frame as having a menu state set
    if self.cc2.Interaction.openChatNextFrame then
        self.cc2.HBChat.TryOpenChat()
        self.cc2.Interaction.openChatNextFrame = false
        return
    end

    -- check if we should return to click mode after chat closes
    if self.cc2.Interaction.lastClickMode then
        if not self.cc2.HBChat.isOpen then
            -- left chat, return to last click mode
            self.cc2.Interaction.ToggleClickMode(self.cc2.Interaction.lastClickMode)
            self.cc2.Interaction.lastClickMode = false
            return -- skip one frame to prevent issues with enter being detected as opening the chat
        end
    end

    -- check if we should abort from click mode
    local controlState = self.cc2.Interaction.CheckControlState()
    if not controlState.valid then
        if self.cc2.Interaction.clickMode then
            -- check if we're trying to open chat
            if controlState.reason and controlState.reason == "enterKey" then
                -- if invalid state was caused by enter keypress, open chat
                self.cc2.Interaction.openChatNextFrame = true
                -- store mode if entering chat and settings allow return to click mode
                if self.cc2.Settings.interaction.returnToClickMode then
                    self.cc2.Interaction.lastClickMode = self.cc2.Interaction.clickMode
                end
            end
        end
        self.cc2.Interaction.ToggleClickMode(false)
        return -- nothing of interest left here if we're not in click mode anymore
    end

    ---------------------------------------------------
    -- click mode activatiion and click registration --
    ---------------------------------------------------

	if Input.GetKeyDown(self.cc2.Settings.keybinds.freeModeToggle) then -- toggle free cursor click mode
		self.cc2.Interaction.ToggleClickMode("free")
	end

	if self.cc2.Interaction.clickMode then
        -- update UI for clickable tooltip
        self.cc2.Interaction.Update()

        -- verify that last clickable we interacted with recieved an 'up' state
        self.cc2.Clickables.CheckLastClickState()

		self.cc2.Clickables.Raycast()

        -- register a click event to happen in the next fixed frame, to prevent jitter issues associated wtih
        if self.cc2.Clickables.currentClickable then -- ugly as shit, easiest way to do it.
            if Input.GetMouseButtonDown(0) then self.cc2.Clickables.OnClick(self.cc2.Clickables.currentClickable, "primary", "down") end
            if Input.GetMouseButtonDown(1) then self.cc2.Clickables.OnClick(self.cc2.Clickables.currentClickable, "secondary", "down") end
            if Input.GetMouseButton(0) then self.cc2.Clickables.OnClick(self.cc2.Clickables.currentClickable, "primary", "hold") end
            if Input.GetMouseButton(1) then self.cc2.Clickables.OnClick(self.cc2.Clickables.currentClickable, "secondary", "hold") end
            if Input.GetMouseButtonUp(0) then self.cc2.Clickables.OnClick(self.cc2.Clickables.currentClickable, "primary", "up") end
            if Input.GetMouseButtonUp(1) then self.cc2.Clickables.OnClick(self.cc2.Clickables.currentClickable, "secondary", "up") end
        end
	end

    ---------------------------------------------------
	-- clcikable visualization
    ---------------------------------------------------

    -- colliders
	if not self.clickables then return end
	for i=1,#self.clickables do
		local clickable = self.clickables[i]
		if clickable.hitbox.transform and not Slua.IsNull(clickable.hitbox.transform) then
			--visualize hitbox
			local hitboxTrans = clickable.hitbox.transform
			local hitboxSize = Vector3(tonumber(clickable.hitbox.boxX),tonumber(clickable.hitbox.boxY),tonumber(clickable.hitbox.boxZ))
			HBBuilder.GGizmo.DrawCube(hitboxTrans.localToWorldMatrix, Vector3.zero, hitboxSize, Color(0,1,0.0,1))
		end
	end
end

ClickableControls2BuilderLua.Clickables = {
	Raycast = function() -- raycast for a clickable
		local self = ClickableControls2BuilderLua
		local layerMask = 64
		local direction = Camera.main.transform.forward
		if self.cc2.Interaction.clickMode == "free" then
			local pcCursor = self.cc2.PlayerController.cursor
			local mousePos = Vector3(pcCursor.Position.x, pcCursor.Position.y*-1+Screen.height, 0) -- using windows API mouse position eliminates some jitter, not that it matters in the builder
			direction = Camera.main:ScreenPointToRay(mousePos).direction -- this appears to be where jitter is caused, as locked mode does not jitter
		end
		local ray, hitinfo = Physics.Raycast(Camera.main.transform.position, direction, Slua.out, 100, layerMask)
		if(ray) then
			-- -- optimisation check. will bail if we're raycasting on the same clickable, chances are we'll raycast to the same clickable a lot, since this runs in fixedUpdate
			if self.Clickables.lastHitinfo then
				if self.Clickables.lastHitinfo.collider == hitinfo.collider then
					self.Clickables.lastHitinfo = hitinfo -- still update last hit info though
					return
				end
			end
			local button = hitinfo.collider:GetComponentInParent("HBButton")
			if not button then return end
			local currentClickable = self:BuildClickableData(button)
			if currentClickable then
				self.cc2.Clickables.currentClickable = currentClickable  -- what we're currently looking at, if we're looking at anything
				self.cc2.Interaction.SetCrosshair()
			end
			self.Clickables.lastHitinfo = hitinfo -- save last hitinfo here, so we can get things like the hit coordinate etc, and do some optimisations
		else
			self.Clickables.lastHitinfo = nil
			if self.cc2.Clickables.currentClickable then
				self.cc2.Clickables.currentClickable = nil
				self.cc2.Interaction.SetCrosshair()
			end
		end
	end,
}

-- stripped down VMS functionality, just enough to register clickables to the manager
ClickableControls2BuilderLua.VMS = {
	colliders = {},
	Cleanup = function()
		local self = ClickableControls2BuilderLua
		for i=1, #self.VMS.colliders do
			local coll = self.VMS.colliders[i]
			if coll and not Slua.IsNull(coll) then
				GameObject.DestroyImmediate(coll)
			end
		end
		self.VMS.colliders = {}
	end,

	-- fake initialisation of vehicle, just setup colliders
	PseudoInit = function()
		local self = ClickableControls2BuilderLua
		self.VMS.Cleanup()
		for button in Slua.iter(HBBuilder.Builder.root:GetComponentsInChildren("HBButton")) do
			local sDataComp = button.gameObject:GetComponent("HBBuilder.StringDataContainer")
			if sDataComp then
				if HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, "clickableControl") == "true" then
					local hitbox = button.gameObject.transform:Find("ClickableHitbox").gameObject
					if hitbox then
						if hitbox:GetComponent(BoxCollider) then -- clean up BoxCollider if it exists
							GameObject.Destroy(hitbox:GetComponent(BoxCollider))
						end
						if hitbox:GetComponent("HBBuilder.StringDataContainer") then
							local sDataComp = hitbox:GetComponent("HBBuilder.StringDataContainer")
							local coll = hitbox:AddComponent(BoxCollider)
							if coll then
								-- add to cleanup list
								table.insert(self.VMS.colliders, coll)
								-- set collider
								local collSize = Vector3(tonumber(HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, "boxX")),tonumber(HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, "boxY")),tonumber(HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, "boxZ")))
								coll.size = collSize
								coll.center = Vector3.zero
								coll.isTrigger = true
								coll.gameObject.layer = 6
							end
						end
					end
				end
			end
		end
	end,
}


-- gets a clickable object and turns it into a clickable class table ready for the manager, modified to use setter/getters
function ClickableControls2BuilderLua:BuildClickableData(clickable)
	if not clickable then return end

	-- metamethod setup so that tables act as getter/setter
	local SetMetaMethods = function(extTable, privTable)
		setmetatable(extTable,
		{
			__index = function (table, key)
				if privTable[key] then
					return privTable[key].get
				end
				return nil
			end,
			__newindex = function (table, key, value)
				if privTable[key] then
					privTable[key].set(value)
				end
			end,
			__pairs = function(table)
				return next, privTable, nil
			end,
		})
	end

	-- build clickable data
	clickableData = {
		["supportedTypes"] = {},
		["object"] = clickable.gameObject,
		["button"] = clickable,
		["hitbox"] = {},
		["params"] = {},
		["moveables"] = {},
		["info"] = {},
		["tags"] = {},
	}

	if clickable.transform:Find("BehaviourDefaults") then
		local defaultsRoot = clickable.transform:Find("BehaviourDefaults")
		for i=0,defaultsRoot.childCount-1 do
			local child = defaultsRoot:GetChild(i)
			table.insert(clickableData.supportedTypes, child.name)
		end
	end

	local sDataComp = clickable.gameObject:GetComponent("HBBuilder.StringDataContainer")
	paramsPriv = {}
	if sDataComp then
		for key in Slua.iter(HBBuilder.BuilderUtils.GetStringDataKeys(sDataComp.stringData)) do
			paramsPriv[key] = {
				["get"] = HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, key),
				["set"] = function(v) paramsPriv[key].get = v; sDataComp.stringData = self.StringData.Set(sDataComp.stringData, key, v) end,
			}
		end
        if ClickableControls2.Clickables.clickableTypes[paramsPriv.type.get] then
            if ClickableControls2.Clickables.clickableTypes[paramsPriv.type.get].init then
                ClickableControls2.Clickables.clickableTypes[paramsPriv.type.get].init(clickableData)

                -- workaround for adding new fiends to getter/setter
                -- TODO this is fucked, i'm tired, see issue #23
                for key,val in pairs(clickableData.params) do
                    paramsPriv[key] = {
        				["get"] = val,
        				["set"] = function(v) paramsPriv[key].get = v; sDataComp.stringData = self.StringData.Set(sDataComp.stringData, key, v) end,
        			}
                end
            end
        end
	end

	SetMetaMethods(clickableData.params, paramsPriv)


	local hitboxPriv = {}
	local sDataComp = clickable.transform:Find("ClickableHitbox").gameObject:GetComponent("HBBuilder.StringDataContainer")
	if sDataComp then
		for key in Slua.iter(HBBuilder.BuilderUtils.GetStringDataKeys(sDataComp.stringData)) do
			hitboxPriv[key] = {
				["get"] = HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, key),
				["set"] = function(v) hitboxPriv[key].get = v; sDataComp.stringData = self.StringData.Set(sDataComp.stringData, key, v) end,
			}
			clickableData.hitbox.transform = clickable.transform:Find("ClickableHitbox")
		end
	end
	SetMetaMethods(clickableData.hitbox, hitboxPriv)

	local index = 0
	for moveable in Slua.iter(clickable.moveables) do
		local sDataComp = moveable.gameObject:GetComponent("HBBuilder.StringDataContainer")
		local moverParams = {}
		moverParams.moveable = {
			["get"] = moveable,
			["set"] = function() end,
		}
		if sDataComp then
			for key in Slua.iter(HBBuilder.BuilderUtils.GetStringDataKeys(sDataComp.stringData)) do
				moverParams[key] = {
					["get"] = HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, key),
					["set"] = function(v) moverParams[key].get = v; sDataComp.stringData = self.StringData.Set(sDataComp.stringData, key, v) end,
				}
			end
		end
		clickableData.moveables[#clickableData.moveables+1] = {}
		SetMetaMethods(clickableData.moveables[#clickableData.moveables], moverParams)
	end

	local infoPriv = {}
	if clickable.gameObject.transform:Find("ClickableInfo") then
		local sDataComp = clickable.gameObject.transform:Find("ClickableInfo").gameObject:GetComponent("HBBuilder.StringDataContainer")
		if sDataComp then
			for key in Slua.iter(HBBuilder.BuilderUtils.GetStringDataKeys(sDataComp.stringData)) do
				infoPriv[key] = {
					["get"] = HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, key),
					["set"] = function(v) infoPriv[key].get = v; sDataComp.stringData = self.StringData.Set(sDataComp.stringData, key, v) end,
				}
			end
		end
	end
	SetMetaMethods(clickableData.info, infoPriv)



    ClickableControls2.Clickables.UpdateMovables(clickableData)
    clickableData.button:UpdateOutputs()

	return clickableData
end

-- custom stringdata handling since stock removes empty keys, which we don't necessarily want to do
ClickableControls2BuilderLua.StringData = {
	Get = function(stringData, key)
		if not stringData then return end
		local ret = {}
		for pair in string.gmatch(stringData, '([^;]+)') do
			local k = string.match(pair, '([^,]+)')
			local v = string.match(pair, '([^,]+)',#k+1) or ""
			if key then
				if k == key then
					return v
				end
			end
			ret[k] = v
		end
		return ret
	end,
	BuildString = function(dataTable)
		if not dataTable then return end
		local ret = ""
		for k,v in pairs(dataTable) do
			ret = ret .. tostring(k) .. "," .. tostring(v) .. ";"
		end
		return ret
	end,
	Set = function(stringData, key, value)
		if not stringData then return end
		if not key then return end
		if not value then value = "" end
		local dataTable = ClickableControls2BuilderLua.StringData.Get(stringData)
		dataTable[key] = value
		local returnString = ClickableControls2BuilderLua.StringData.BuildString(dataTable)
		return returnString
	end,
}

-- build the inspector UI fields
function ClickableControls2BuilderLua:BuildWizardData(clickables, seats)
	if not (clickables or seats) then return end
	--init vars
	local ret = {};
	local groupedParts = {}

	local partGroup = {}
	local sumNameCheck = ""
	local canMultiEdit = true
	local gotPart = false
	local partName = "Inspector"

	local name = ""
	if clickables[1] then
		name = clickables[1].object.name
	else
		name = seats[1].name
	end
	local headerItem = {
		name         = name,
		tooltip      = "Part Proprties",
		uiType       = "headerProperty",
		layout       = {"min",Vector2(0,30)}
    }
	if #clickables > 1 or #seats > 1then
		headerItem.name = "Clickable Control Multi Edit"
	end
    table.insert(ret,headerItem)

	local groupIDs = {}

	for i=1, #seats do
		local seat = seats[i]
			-- collision avoidance for gruipIDs
			local groupID = seat.name
			if groupIDs[groupID] then
				local i2 = 2
				local collisionName = groupID
				while groupIDs[collisionName] do
					collisionName = groupID .. " " .. tostring(i2)
					i2 = i2 + 1
					-- emergency break
					if i2 > 1000 then break end
				end
				groupID = collisionName
			end
			groupIDs[groupID] = true

			-------------------------
			-- State tags
			-------------------------
			local tags = {}
			local tagIndex = 1
			if seat.transform:Find("StateTags") then
				local sDataComp = seat.transform:Find("StateTags").gameObject:GetComponent("HBBuilder.StringDataContainer")
				if sDataComp then
					for key in Slua.iter(HBBuilder.BuilderUtils.GetStringDataKeys(sDataComp.stringData)) do
						local tag = tostring(HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, key))
						tags[#tags+1] = {key, tag}
					end
				end
			end

			local AddStateObject = function()
				if not seat.transform:Find("StateTags") then
					local stateTagObject = GameObject("StateTags")
					stateTagObject.transform.parent = seat.transform
					stateTagObject.transform.localPosition = Vector3.zero
					stateTagObject:AddComponent("HBBuilder.StringDataContainer")
				end
			end

			for i=1, #tags do
				local tag = tags[i] or ""
				local item = {
					name = "State Tag",
					tooltip = "Use state tags to control what clickables this seat can use",
					uiType = "stringProperty",
					groupID = groupID,
					arrayBehavior = true,
					value = tostring(tag[2]),
					func = function(v)
						if not v then return end
						if v == "" then
							tags[i] = nil
						end

						-- add stateTag object if necessary
						AddStateObject(seat)

						tag[2] = tostring(v)
						local tagIndex = 1
						local stringData = ""
						for k, tag in pairs(tags) do
							stringData = self.StringData.Set(stringData, "stateTag" .. tostring(tagIndex), tostring(tag[2]))
							tagIndex = tagIndex + 1
						end
						seat.transform:Find("StateTags").gameObject:GetComponent("HBBuilder.StringDataContainer").stringData = stringData

						if v == "" then
							self:OnInspectorChanged()
						end
					end,

					arrayAddFunc = function()
						AddStateObject()

						local tagIndex = 1
						local stringData = ""
						for k, tag in pairs(tags) do
							stringData = self.StringData.Set(stringData, "stateTag" .. tostring(tagIndex), tostring(tag[2]))
							tagIndex = tagIndex + 1
						end

						-- add new empty
						stringData = self.StringData.Set(stringData, "stateTag" .. tostring(tagIndex), nil)

						seat.transform:Find("StateTags").gameObject:GetComponent("HBBuilder.StringDataContainer").stringData = stringData
						self:OnInspectorChanged()
					end,

					arrayRemoveFunc = function()
						tags[i] = nil
						local tagIndex = 1
						local stringData = ""
						for k, tag in pairs(tags) do
							stringData = self.StringData.Set(stringData, "stateTag" .. tostring(tagIndex), tostring(tag[2]))
							tagIndex = tagIndex + 1
						end
						seat.transform:Find("StateTags").gameObject:GetComponent("HBBuilder.StringDataContainer").stringData = stringData
						self:OnInspectorChanged()
					end,
					arrayRenameFunc = function() end,
				}
				table.insert(ret,item)
			end

			if #tags < 1 then
				local item = {
					name = "Enable State Tags",
					tooltip = "Enable State Tags to set what clickables this seat may control",
					uiType = "buttonProperty",
					minValue = 0,
					maxValue = 0,
					groupID = groupID,
					buttonText = "Enable",
					func = function()
						AddStateObject()
						local stringData = ""
						stringData = self.StringData.Set(stringData, "stateTag" .. tostring(1), nil)
						seat.transform:Find("StateTags").gameObject:GetComponent("HBBuilder.StringDataContainer").stringData = stringData
						self:OnInspectorChanged()
					end,
				}
				table.insert(ret,item)
			end
	end

    for i=1, #clickables do
		local clickable = clickables[i]
		local canEdit = true
		if clickable.params.canEdit then
			if clickable.params.canEdit == "false" then
				canEdit = false
			end
		end

		if canEdit then -- skip if not editable
			-- collision avoidance for gruipIDs
			local groupID = clickable.object.name
			if groupIDs[groupID] then
				local i2 = 2
				local collisionName = groupID
				while groupIDs[collisionName] do
					collisionName = groupID .. " " .. tostring(i2)
					i2 = i2 + 1
					-- emergency break
					if i2 > 1000 then break end
				end
				groupID = collisionName
			end
			groupIDs[groupID] = true

			-------------------------
			-- Type changing
			-------------------------

			if #clickable.supportedTypes > 0 then
				-- Type enum
				local item = {
					name = "Clickable Type",
					tooltip = "The behaviour of this clickable",
					uiType = "enumProperty",
					-- minValue = 0,
					-- maxValue = 0,
					groupID = groupID,
					options = function()
						local ret = {}
						for k, type in pairs(clickable.supportedTypes) do
							ret[#ret+1] = ClickableControls2.Clickables.clickableTypes[type].displayName or type
						end
						return ret
					end,
					value = function()
						local ret = clickable.params.type
						if ClickableControls2.Clickables.clickableTypes[ret] then
							return ClickableControls2.Clickables.clickableTypes[ret].displayName or ret
						end
						return ret
					end,
					func = function(v)
						local internalType = self.clickableTypeReverseLookup[v]
						if not internalType then internalType = v end
						local defaultsRoot = clickable.object.transform:Find("BehaviourDefaults")
						if not defaultsRoot then return end
						local defaultsChild = defaultsRoot:Find(internalType)
						if not defaultsChild then return end
						clickable.object:GetComponent("HBBuilder.StringDataContainer").stringData = defaultsChild.gameObject:GetComponent("HBBuilder.StringDataContainer").stringData
						ClickableControls2BuilderLua:OnInspectorChanged() -- workaround to force refresh
					end,
				}
				table.insert(ret,item)
			end

			-------------------------
			-- Clickable info
			-------------------------

			-- Clickable name
			local item = {
				name = "Display Name",
				tooltip = "The UI name of this Clickable",
				uiType = "stringProperty",
				minValue = 0,
				maxValue = 0,
				groupID = groupID,
				value = clickable.info.name,
				func = function(v) clickable.info.name = tostring(v) end,
			}
			table.insert(ret,item)

			-- Clickable tooltip
			local item = {
				name = "Display Tooltip",
				tooltip = "The UI tooltip of this Clickable",
				uiType = "stringProperty",
				minValue = 0,
				maxValue = 0,
				groupID = groupID,
				value = clickable.info.tooltip,
				func = function(v) clickable.info.tooltip = tostring(v) end,
			}
			table.insert(ret,item)

			-- scale
			local item = {
				name = "Scale",
				tooltip = "The overall scale of the clickable object",
				uiType = "floatProperty",
				minValue = 0,
				maxValue = 0,
				groupID = groupID,
				value = clickable.object.transform.localScale.x,
				func = function(v)
				  clickable.object.transform.localScale = Vector3(v, v, v)
				end,
			}
			table.insert(ret,item)

			-------------------------
			-- parameters
			-------------------------
			if ClickableControls2.Clickables.clickableTypes[clickable.params.type] then
				local parameterKeys = ClickableControls2.Clickables.clickableTypes[clickable.params.type].params
				for k,paramKey in pairs(parameterKeys) do
					if clickable.params[paramKey.name] then
						local key = paramKey.name
						local item = {
							name = paramKey.displayName,
							tooltip = paramKey.tooltip or "",
							uiType = paramKey.uiType,
							minValue = paramKey.minValue or nil,
							maxValue = paramKey.maxValue or nil,
							groupID = groupID,
							value = tonumber(clickable.params[key]),
							func = function(v)
								clickable.params[key] = v
								for i=1, #clickable.moveables do
									local moveable = clickable.moveables[i]
									local stateVal = ClickableControls2.InverseLerp(tonumber(clickable.params.min), tonumber(clickable.params.max), 0)
									ClickableControls2.Clickables.moveableTypes[moveable.type].func(moveable, stateVal)
								end
							end,
						}
						table.insert(ret,item)
					end
				end
			end

			-------------------------
			-- moveables
			-------------------------
			for i=1, #clickable.moveables do
				local moveable = clickable.moveables[i]
				local canEdit = true
				if moveable.canEdit then
					if moveable.canEdit == "false" then
						canEdit = false
					end
				end

				if canEdit then -- skip if not editable
					if ClickableControls2.Clickables.moveableTypes[moveable.type] then
						local parameterKeys = ClickableControls2.Clickables.moveableTypes[moveable.type].params
						for k,paramKey in pairs(parameterKeys) do
							if moveable[paramKey.name] then
								local key = paramKey.name
								local item = {
									name = "(Moveable)    " .. paramKey.displayName,
									tooltip = paramKey.tooltip or "",
									uiType = paramKey.uiType,
									minValue = paramKey.minValue or nil,
									maxValue = paramKey.maxValue or nil,
									groupID = groupID,
									value = tonumber(moveable[key]),
									func = function(v)
										moveable[key] = v
										local stateVal = ClickableControls2.InverseLerp(tonumber(clickable.params.min), tonumber(clickable.params.max), 0)
										ClickableControls2.Clickables.moveableTypes[moveable.type].func(moveable, stateVal)
									end,
								}
								table.insert(ret,item)
							end
						end
					end
				end
			end

			-------------------------
			-- State tags
			-------------------------
			local tags = {}
			local tagIndex = 1
			if clickable.object.transform:Find("StateTags") then
				local sDataComp = clickable.object.transform:Find("StateTags").gameObject:GetComponent("HBBuilder.StringDataContainer")
				if sDataComp then
					for key in Slua.iter(HBBuilder.BuilderUtils.GetStringDataKeys(sDataComp.stringData)) do
						local tag = tostring(HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, key))
						tags[#tags+1] = {key, tag}
					end
				end
			end

			local AddStateObject = function()
				if not clickable.object.transform:Find("StateTags") then
					local stateTagObject = GameObject("StateTags")
					stateTagObject.transform.parent = clickable.object.transform
					stateTagObject.transform.localPosition = Vector3.zero
					stateTagObject:AddComponent("HBBuilder.StringDataContainer")
				end
			end

			for i=1, #tags do
				local tag = tags[i] or ""
				local item = {
					name = "State Tag",
					tooltip = "Use state tags to control where the clickable can be used from.",
					uiType = "stringProperty",
					groupID = groupID,
					arrayBehavior = true,
					value = tostring(tag[2]),
					func = function(v)
						if not v then return end
						if v == "" then
							tags[i] = nil
						end

						-- add stateTag object if necessary
						AddStateObject(clickable)

						tag[2] = tostring(v)
						local tagIndex = 1
						local stringData = ""
						for k, tag in pairs(tags) do
							stringData = self.StringData.Set(stringData, "stateTag" .. tostring(tagIndex), tostring(tag[2]))
							tagIndex = tagIndex + 1
						end
						clickable.object.transform:Find("StateTags").gameObject:GetComponent("HBBuilder.StringDataContainer").stringData = stringData

						if v == "" then
							self:OnInspectorChanged()
						end
					end,

					arrayAddFunc = function()
						AddStateObject()

						local tagIndex = 1
						local stringData = ""
						for k, tag in pairs(tags) do
							stringData = self.StringData.Set(stringData, "stateTag" .. tostring(tagIndex), tostring(tag[2]))
							tagIndex = tagIndex + 1
						end

						-- add new empty
						stringData = self.StringData.Set(stringData, "stateTag" .. tostring(tagIndex), nil)

						clickable.object.transform:Find("StateTags").gameObject:GetComponent("HBBuilder.StringDataContainer").stringData = stringData
						self:OnInspectorChanged()
					end,

					arrayRemoveFunc = function()
						tags[i] = nil
						local tagIndex = 1
						local stringData = ""
						for k, tag in pairs(tags) do
							stringData = self.StringData.Set(stringData, "stateTag" .. tostring(tagIndex), tostring(tag[2]))
							tagIndex = tagIndex + 1
						end
						clickable.object.transform:Find("StateTags").gameObject:GetComponent("HBBuilder.StringDataContainer").stringData = stringData
						self:OnInspectorChanged()
					end,
					arrayRenameFunc = function() end,
				}
				table.insert(ret,item)
			end

			if #tags < 1 then
				local item = {
					name = "Enable State Tags",
					tooltip = "Enable state tags to control where the clickable can be used from.",
					uiType = "buttonProperty",
					minValue = 0,
					maxValue = 0,
					groupID = groupID,
					buttonText = "Enable",
					func = function()
						AddStateObject()
						local stringData = ""
						stringData = self.StringData.Set(stringData, "stateTag" .. tostring(1), nil)
						clickable.object.transform:Find("StateTags").gameObject:GetComponent("HBBuilder.StringDataContainer").stringData = stringData
						self:OnInspectorChanged()
					end,
				}
				table.insert(ret,item)
			end

		end
    end
    return ret
end

-- TODO clickable types able to call special OnEdit function after editing
ClickableControls2BuilderLua.ClickableOnEdit = {

}

-- recursively gets components of type from children up to a limited depth
function ClickableControls2BuilderLua:GetComponentsInChildrenLimited(obj, type, depthMax, depth)
	if not obj then return end
	if not type then return end
	if not depthMax then return end
	if not depth then depth = 0 end
	local comps = {}
	for comp in Slua.iter(obj:GetComponents(type)) do
		table.insert(comps, comp)
	end
	if depth < depthMax then
		-- get components from children
		for i=0,obj.transform.childCount-1 do
			local child = obj.transform:GetChild(i)
			local childComps = self:GetComponentsInChildrenLimited(child.gameObject, type, depthMax, depth+1)
			if childComps then
				for i2=1,#childComps do
					local childComp = childComps[i2]
					table.insert(comps, childComp)
				end
			end
		end
	end
	return comps
end


-- seat clickable routines TODO: finish seat tag system
ClickableControls2BuilderLua.ClickableSeat = {
	IsSeat = function(obj)
		for i=0, obj.transform.childCount-1 do
			if not Slua.IsNull(obj.transform:GetChild(i):GetComponent("HBSeat")) then
				return obj.transform:GetChild(i).gameObject
			end
		end
		return
	end,
}

return ClickableControls2BuilderLua
