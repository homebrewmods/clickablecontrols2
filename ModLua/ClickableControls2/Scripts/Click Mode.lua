NoBinds={}

function NoBinds:Awake()
	self.active = true
end

function NoBinds:Update()
end

function NoBinds:OnDestroy()
	self.active = false
end

return NoBinds