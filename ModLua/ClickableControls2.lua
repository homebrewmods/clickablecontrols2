ClickableControls2 = {}
-- By RhoSigma

ClickableControls2.version = "1.23 27/01/2021"
ClickableControls2.enabled = true

ClickableControls2.Settings = {
	clickRange = 2, -- how far we can reach by default
	infoBoxDelayFree = 1, -- seconds looking at a clickable before info box opens while in free mouse mode
	infoBoxDelayLocked = 1.5, -- seconds looking at a clickable before info box opens while in locked mouse mode
	keybinds = {
		lockedModeToggle = KeyCode.C, -- button to toggle locked cursor clickable mode on/off
		freeModeToggle = KeyCode.Z, -- button to toggle free cursor clickable mode on/off
	},
	playerController = {
		xMargin = 0.2, -- distance from of screen that camera movement begins in free mouse mode, colin likes 0.2, nata likes 0.375
		yMargin = 0.2, -- distance from edge of screen that camera movement begins in free mouse mode, colin likes 0.2, nata likes 0.35
		lookSensitivity = 1, -- look sensitivity coefficient when in free look mode
		autoReturnCursor = true, -- should cursor move out of margin while turning?
		xReturnSpeed = 20,	-- speed at which cursor returns from x margin
		yReturnSpeed = 10,	-- speed at which cursor returns from y margin
	},
	clickables = {
		maxSeatTags = 10, -- max number of state tags a seat can have
	},
	interaction = {
		returnToClickMode = true, -- automatically return to click mode after pressing enter to open chat
	},
	networking = {
		clickEventRateLimit = 0.05, -- min time between click up/down events synced
		holdEventRateLimit = 0.1, -- min time between hold events synced
		backgroundSyncRateFast = 0.1, -- time between background sync events if player recently clicked
		backgroundSyncRateSlow = 2, -- time between background sync events if player has not clicked recently
		playerActivityCooldown = 5, -- time taken to switch from fast to slow sync
		syncMasterCooldown = 2, -- the time taken for the clickable to allow a new sync master
	},
}

-------------------------------------------------------------------------------------------------
-- First pass init
-------------------------------------------------------------------------------------------------
ClickableControls2.IDManager = Slua.GetClass("IDManager")
ClickableControls2.MainMenuClass = Slua.GetClass("MainMenu")
ClickableControls2.player = HBU.player
ClickableControls2.HBPlayer = HBU.player:GetComponentInChildren("HBPlayer")
ClickableControls2.HBChat = GameObject.FindObjectOfType("HBChat")
ClickableControls2.resourceDir = HBU.GetModLuaFolder().."/ClickableControls2"

ClickableControls2.sw = Slua.CreateClass("System.Diagnostics.Stopwatch") --ClickableControls2.sw.Reset();ClickableControls2.sw.Start()		ClickableControls2.sw.Stop();Debug.Log(ClickableControls2.sw.Elapsed)

function ClickableControls2:Awake()
	Debug.Log("Initialised Clickable Controls 2 V " .. tostring(self.version))
end

function ClickableControls2:Update()
	if not self.enabled then return end
	-- stripped out functionality while in builder
	if HBU.InBuilder() then
		self.Interaction.ToggleClickMode(false)
		return
	end

	-- update networking
	self.Networking.Update()

	-- TODO move this and new objs to RhoUtils
	-- check player vehicle/foot status
	self.Interaction.GetPlayerStatus()

	-- TODO move this and new objs to RhoUtils
	-- new vehicle scan and init
	ClickableControls2.FindNewObjects:Update()
	for k,v in ipairs(ClickableControls2.FindNewObjects.NewVehicles) do
		ClickableControls2.VMS.RegisterForVMS(v)
	end
	-- VMS queue holds vehicles that are still spawning until they finish, otherwise CC2 will break them
	if #self.VMS.VMSqueue > 0 then
		ClickableControls2.VMS.Update()
	end

	------------------------------------------------------------------
	-- valid state checking and auto chat open/click mode returning --
	------------------------------------------------------------------

	-- check if we should open chat this frame, workaround for issues arising from trying to open chat same frame as having a menu state set
	if self.Interaction.openChatNextFrame then
		self.HBChat.TryOpenChat()
		self.Interaction.openChatNextFrame = false
		return
	end

	-- check if we should return to click mode after chat closes
	if self.Interaction.lastClickMode then
		if not self.HBChat.isOpen then
			-- left chat, return to last click mode
			self.Interaction.ToggleClickMode(self.Interaction.lastClickMode)
			self.Interaction.lastClickMode = false
			return -- skip one frame to prevent issues with enter being detected as opening the chat
		end
	end

	-- check if we should abort from click mode
	local controlState = self.Interaction.CheckControlState()
	if not controlState.valid then
		if self.Interaction.clickMode then
			-- check if we're trying to open chat
			if controlState.reason and controlState.reason == "enterKey" then
				-- if invalid state was caused by enter keypress, open chat
				self.Interaction.openChatNextFrame = true
				-- store mode if entering chat and settings allow return to click mode
				if self.Settings.interaction.returnToClickMode then
					self.Interaction.lastClickMode = self.Interaction.clickMode
				end
			end
		end
		self.Interaction.ToggleClickMode(false)
		return -- nothing of interest left here if we're not in click mode anymore
	end

	---------------------------------------------------
	-- click mode activatiion and click registration --
	---------------------------------------------------

	-- toggle click mode
	if Input.GetKeyDown(self.Settings.keybinds.lockedModeToggle) then -- toggle locked cursor click mode
		self.Interaction.ToggleClickMode("locked")
	end
	if Input.GetKeyDown(self.Settings.keybinds.freeModeToggle) then -- toggle free cursor click mode
		self.Interaction.ToggleClickMode("free")
	end

	if self.Interaction.clickMode then
		-- update player movement and cursor while in free click mode
		if self.Interaction.clickMode == "free" then
			self.PlayerController.Update()
		end

		-- update UI for clickable tooltip
		self.Interaction.Update()

		-- verify that last clickable we interacted with recieved an 'up' state
		self.Clickables.CheckLastClickState()

		-- register a click event to happen in the next fixed frame, to prevent jitter issues due to raycasting in a different tick to physics movement
		if self.Clickables.currentClickable then -- ugly as shit, easiest way to do it.
			if Input.GetMouseButtonDown(0) then self.Clickables.RegisterClick(self.Clickables.currentClickable, "primary", "down") end
			if Input.GetMouseButtonDown(1) then self.Clickables.RegisterClick(self.Clickables.currentClickable, "secondary", "down") end
			if Input.GetMouseButton(0) then self.Clickables.RegisterClick(self.Clickables.currentClickable, "primary", "hold") end
			if Input.GetMouseButton(1) then self.Clickables.RegisterClick(self.Clickables.currentClickable, "secondary", "hold") end
			if Input.GetMouseButtonUp(0) then self.Clickables.RegisterClick(self.Clickables.currentClickable, "primary", "up") end
			if Input.GetMouseButtonUp(1) then self.Clickables.RegisterClick(self.Clickables.currentClickable, "secondary", "up") end
		end
	end
end

function ClickableControls2:FixedUpdate()
	-- clickable raycasting needs to happen in fixedUpdate, as the rendered frame and the clickable physical location would be out of sync in Update()
	if self.Interaction.clickMode then
		self.Clickables.Raycast() -- Raycast() will return the clickable we're looking at in the most optimised way
		 -- queuedClick will be set if we hit a valid clickable in Raycast() and a click event was registered with RegisterClick() in Update()
		 -- this event will only fire at the same rate as Update() despite being in FixedUpdate(), as RegisterClick() must renew the click request every Update() frame
		if self.Clickables.queuedClick then
			local clickData = self.Clickables.queuedClick
			-- OnClick() fires the actual clickable function according to the click event data
			self.Clickables.OnClick(clickData.clickable, clickData.button, clickData.state)
			self.Clickables.queuedClick = nil
		end
	end
end

function ClickableControls2:OnDestroy()
	if self.Interaction.crosshairUI then
		GameObject.Destroy(self.Interaction.crosshairUI)
	end
end

-- PlayerController handles player/camera movement while in free mouse mode
ClickableControls2.PlayerController = {
	-- cursor stuff, we need to use windows forms since the unity controller can't get cursor fast enough
	cursorPoint = Slua.CreateClass("System.Drawing.Point", 0, 0),
	cursor = Slua.GetClass("System.Windows.Forms.Cursor"),

	-- get HB built in player controller, since we need to make modifications to it
	playerController = GameObject.FindObjectOfType("player_input_controller"),

	lostFocus = false, -- track application focus so we don't fuck with the cursor while user is alt-tabbed
	Update = function()
		local self = ClickableControls2

		-- return mouse to original position upon reentering focus
		if Application.isFocused then
			if self.PlayerController.lostFocus then
				if self.PlayerController.lastCursorPos then
					ClickableControls2.PlayerController.cursor.Position = self.PlayerController.lastCursorPos
					self.PlayerController.lastCursorPos = nil
				end
				self.PlayerController.lostFocus = false
			else
				self.PlayerController.lastCursorPos = ClickableControls2.PlayerController.cursor.Position
			end
		else
			self.PlayerController.lostFocus = true
		end

		-- manage player controller
		local xAxis = self.PlayerController.GetXMovement()
		local yAxis = self.PlayerController.GetYMovement()
		self.PlayerController.UpdatePlayerController(xAxis, yAxis)
	end,
	GetXMovement = function()
		local self = ClickableControls2
		if not self then return end
		if not Application.isFocused then return 0 end
		local xMargin = math.min(math.max(self.Settings.playerController.xMargin*Screen.width, 1), Screen.width-1)
		local xAxis = 0
		if Input.mousePosition.x >= Screen.width-xMargin then
			xAxis = (Input.mousePosition.x-(Screen.width-xMargin))/xMargin
		elseif Input.mousePosition.x <= xMargin then
			xAxis = (Input.mousePosition.x-xMargin)/xMargin
		end
		xAxis = xAxis * HomebrewManager.lookSensitivity * self.Settings.playerController.lookSensitivity

		-- return cursor from margins
		if not self.Settings.playerController.autoReturnCursor then return xAxis end
		local pc = ClickableControls2.PlayerController
		if pc.cursorPoint and pc.cursor then
			pc.cursorPoint.x = pc.cursor.Position.x + (xAxis*self.Settings.playerController.xReturnSpeed*-1)
			pc.cursorPoint.y = pc.cursor.Position.y
			pc.cursor.Position = pc.cursorPoint
		end
		return xAxis
	end,
	GetYMovement = function()
		local self = ClickableControls2
		if not self then return end
		if not Application.isFocused then return 0 end
		local yMargin = math.min(math.max(self.Settings.playerController.yMargin*Screen.height, 1), Screen.height-1)
		local yAxis = 0
		if Input.mousePosition.y >= Screen.height-yMargin then
			yAxis = (Input.mousePosition.y-(Screen.height-yMargin))/yMargin
		elseif Input.mousePosition.y <= yMargin then
			yAxis = (Input.mousePosition.y-yMargin)/yMargin
		end
		yAxis = yAxis * HomebrewManager.lookSensitivity * self.Settings.playerController.lookSensitivity

		-- return cursor from margins
		if not self.Settings.playerController.autoReturnCursor then return yAxis end
		local pc = ClickableControls2.PlayerController
		if pc.cursorPoint and pc.cursor then
			pc.cursorPoint.x = pc.cursor.Position.x
			pc.cursorPoint.y = pc.cursor.Position.y + (yAxis*self.Settings.playerController.yReturnSpeed)
			pc.cursor.Position = pc.cursorPoint
		end
		return yAxis
	end,
	-- reimplementation of character motor, allows us to control the character like normal even though HBU.SetInMenu() is called
	UpdatePlayerController = function(xInput, yInput)
		local playerMotor = ClickableControls2.PlayerController.playerController
		if not xInput then xInput = 0 end
		if not yInput then yInput = 0 end
		-- direction

		local ClampAngle = function(angle, min, max)
			if angle <= min then return angle + math.abs(min)
			elseif angle >= max then return angle - math.abs(max) end
			return angle
		end
		playerMotor.rotation_x = playerMotor.rotation_x + (xInput * HomebrewManager.lookSensitivity * (math.max(0.1, Camera.main.fieldOfView) / HomebrewManager.fieldOfView))
		playerMotor.rotation_x = ClampAngle(playerMotor.rotation_x, -360, 360)
		local rhs = Quaternion.AngleAxis(playerMotor.rotation_x, Vector3.up)

		local invertY = 1
		if HBU.InvertY then invertY = -1 end
		playerMotor.rotation_y = playerMotor.rotation_y + (yInput * HomebrewManager.lookSensitivity * (math.max(0.1, Camera.main.fieldOfView) / HomebrewManager.fieldOfView) * invertY)
		playerMotor.rotation_y = math.min(math.max(playerMotor.rotation_y, -85), 85)
		local rhs2 = Quaternion.AngleAxis(playerMotor.rotation_y, Vector3.right * -1)
		playerMotor.playerPivot.localRotation = Quaternion.identity * rhs
		playerMotor.headPivot.localRotation = Quaternion.identity * rhs2

		-- movement
		local directionVector = Vector3.zero
		directionVector = Vector3(playerMotor.strafe:GetKey(false), 0, playerMotor.move:GetKey(false))
		directionVector = directionVector.normalized * math.min(1, directionVector.magnitude)
		directionVector = playerMotor.playerPivot:TransformDirection(directionVector)
		if playerMotor.run ~= nil and playerMotor.run:GetKey(false) > 0.5 then
			directionVector = directionVector * 1.5
			if not HBU.InSeat() and playerMotor.motor.grounded then
				playerMotor.walkLength = playerMotor.walkLength + (playerMotor.transform.position - playerMotor.prePos).magnitude
				playerMotor.prePos = playerMotor.transform.position
				if playerMotor.walkLength > playerMotor.runstepLength then
					playerMotor.PlayFootstep()
					playerMotor.walkLength = 0
				end
			end
		elseif not HBU.InSeat() and playerMotor.motor.grounded then
			playerMotor.walkLength = playerMotor.walkLength + (playerMotor.transform.position - playerMotor.prePos).magnitude
			playerMotor.prePos = playerMotor.transform.position
			if playerMotor.walkLength > playerMotor.footstepLength then
				playerMotor.PlayFootstep()
				playerMotor.walkLength = 0
			end
		end
		playerMotor.motor.inputMoveDirection = directionVector
		if playerMotor.jump then
			playerMotor.motor.inputJump = playerMotor.jump:GetKey(false) > 0.5
			if playerMotor.jump:GetKeyDown(false) then
				playerMotor.motor.Jump()
			end
		end
	end,
}

-- Interaction handles everything to do with linking the player to the clickables system, including the UI, player state checks, event handling, etc
ClickableControls2.Interaction = {
	-- player state stuff

	-- player status is a lookup table, stores states such as 'onFoot', 'seated', 'owner', or any tags attached to releavant seat
	playerStatus = {},
	GetPlayerStatus = function()
		local self = ClickableControls2
		if not self.player.transform.parent or Slua.IsNull(self.player.transform.parent) then -- player on foot
			if not self.Interaction.playerStatus.onFoot then -- looks like we exited a vehicle
				self.Interaction.lastSeat = nil -- clear last known seat

				-- set status tags
				self.Interaction.ClearPlayerStatus()
				self.Interaction.playerStatus.onFoot = true
				self.Interaction.playerStatus.seated = false

				self.Interaction.OnStatusChanged()
			end
			return
		end
		-- player in vehicle
		if self.Interaction.lastSeat then
			if self.Interaction.lastSeat == self.player.transform.parent then
				return -- seat not changed
			end
		end
		self.Interaction.lastSeat = self.player.transform.parent -- update last known seat

		-- set status tags
		self.Interaction.ClearPlayerStatus()
		self.Interaction.playerStatus.onFoot = false
		self.Interaction.playerStatus.seated = true

		-- seat changed
		local stateTagObject = HBU.player.transform.parent.parent:Find("StateTags")
		if stateTagObject then
			local sDataComp = stateTagObject.gameObject:GetComponent("HBBuilder.StringDataContainer")
			-- quickly scan for possible number of seatTags
			local tagCount = 0
			for tag in string.gmatch(sDataComp.stringData, 'stateTag%d') do
				tagCount = tagCount + 1
			end

			-- find all stringData with key 'stringData<digit>' and add respective tag to status
			for i=1, math.min(self.Settings.clickables.maxSeatTags, tagCount) do
				local tag = HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, "stateTag" .. tostring(i))
				if not tag then break end -- probably shouldn't hit this, but the user may have named a seatTag 'seatTag<digit>' which could cause a miscount
				if tag ~= "" then
					self.Interaction.playerStatus[tostring(tag)] = true
				end
			end
		end
		self.Interaction.OnStatusChanged()
	end,

	ClearPlayerStatus = function()
		local self = ClickableControls2
		self.Interaction.playerStatus = {
			["onFoot"] = true,
			["seated"] = false,
			-- ["owner"] = false,
		}
	end,

	-- called when the player status changes (gets in/out of seat, changes seat)
	OnStatusChanged = function()
		local self = ClickableControls2
		self.VMS.VMSStatusRefresh()
	end,

	-- check menu state for manual menu override, returns valid, reason
	-- valid will be true if click mode should be allowed, reason will be given if false
	CheckControlState = function()
		local self = ClickableControls2

		-- are we trying to enter chat?
		if Input.GetKeyDown(KeyCode.Return)
		then
			return {valid = false, reason = "enterKey"}
		end

		-- any keys down that should drop us out of click mode?
		-- TODO try to change these to bind from settings or something. if a player rebinds any of these functions things will break
		if Input.GetKeyDown(KeyCode.Escape)
		or Input.GetKeyDown(KeyCode.Tab)
		or Input.GetKeyDown(KeyCode.F1)
		-- are we in 3rd person?
		or not self.HBPlayer.inFirstPerson
		then
			return {valid = false, reason = "blacklistKey"}
		end

		-- any menu states that should drop us out of click mode?
		local mm = self.MainMenuClass
		if mm.onMainMenu
		or mm.inSave
		or mm.inKeyAssigner
		or mm.inBrowser
		or mm.noControl
		or mm.inUpload
		or mm.inChat
		then
			return {valid = false, reason = "menuState"}
		end

		-- seems like we can safely enter click mode
		return {valid = true}
	end,

	-- clicking stuff
	clickMode = false, -- clickMode is a string type variable, if nil or false no click mode is active, otherwise 'free' or 'locked'
	lastClickMode = false,
	ToggleClickMode = function(mode)
		local self = ClickableControls2
		if not self then return end

		-- bail if requested false and already false
		if not self.Interaction.clickMode then
			if not mode then
				return
			end
		end

		-- toggle off if requested mode is same as current mode
		if self.Interaction.clickMode == mode then
			self.Interaction.clickMode = false
		else
			self.Interaction.clickMode = mode
		end

		if not self.Interaction.crosshairUI then -- init UI if it wasn't already
			if not self.Interaction.InitCrosshair(self) then error("ClickableControls2: ToggleClickMode failed to initialse crosshair") end
		end

		if self.Interaction.clickMode then -- show UI
			self.Interaction.crosshairUI:SetActive(true)

			-- workaround fucking dogshit inventory being impossible to disable, who would ever want to do that right?
			if not self.Interaction.inventoryItems then  -- check to make sure we haven't already written to inventoryItems otherwise we'll clear it completely
				self.Interaction.inventoryItems = Inventory.Items
				Inventory.Items = nil
			end

			-- force gadget swap to empty gadget because there's no other fucking way to disable keybinds or current gadget
			self.Interaction.currentGadget = HBU.curGadgetAssetPath
			Inventory:SelectGadget(self.resourceDir .. "/Scripts/Click Mode.lua") -- doesn't even fucking work
		else -- exit click mode and hide UI
			self.Clickables.Clear() -- clear clickable memory
			self.Clickables.CheckLastClickState() -- verify that last clickable we interacted with recieved an 'up' state

			self.Interaction.crosshairUI:SetActive(false) -- clear UI
			self.Interaction.infoBox:SetActive(false)
			self.Interaction.nameBox:SetActive(false)
			self.Interaction.clickableIndicator:SetActive(false)

			if self.Interaction.inventoryItems then -- check if inventoryItems exists for safety
				Inventory.Items = self.Interaction.inventoryItems -- replace inventory items to reenable inventory. yep. that's the hack.
				self.Interaction.inventoryItems = nil
			end

			Inventory:SelectGadget(self.Interaction.currentGadget) -- return to original gadget from before we had to swap to an empty one. kill me
			return
		end

		if self.Interaction.clickMode == "locked" then -- return crosshair to centre
			self.Interaction.crosshair.transform.position = Vector3(Screen.width/2, Screen.height/2, 0)
			return
		end
	end,

	Update = function()
		local self = ClickableControls2
		if not self then return end

		-- update crosshair with info box
		if self.Interaction.clickMode then
			if self.Clickables.currentClickable then
				-- crosshair updating
				if self.Interaction.crosshairSetTime then
					local tooltipDisplayTime = 0
					-- set time when tooltip will show, tooltip should display slower in locked mode
					if self.Interaction.clickMode == "free" then
						tooltipDisplayTime = self.Interaction.crosshairSetTime + self.Settings.infoBoxDelayFree
					else
						tooltipDisplayTime = self.Interaction.crosshairSetTime + self.Settings.infoBoxDelayLocked
					end
					if Time.time > tooltipDisplayTime then -- enable infobox after delay
						self.Interaction.infoBox:SetActive(true)
						self.Interaction.infoText.text = self.Clickables.currentClickable.info.tooltip
						self.Interaction.crosshairSetTime = nil -- set to nil to prevent Update() from constantly setting the above
						-- force the vertical layout and contentsizefitter to update immediately, so the text fits on the background
						LayoutRebuilder.ForceRebuildLayoutImmediate(self.Interaction.infoBox.transform)
					end
				end
			end

			-- free cursor handling
			if self.Interaction.clickMode == "free"  then
				HBU.SetInMenu()
				self.Interaction.crosshair.transform.position = Input.mousePosition
			end
		end
	end,

	-- crosshair, clickable name, and tooltip stuff
	SetCrosshair = function()
		local self = ClickableControls2
		if not self then return end

		-- init crosshair if it doesn't exist
		if not self.Interaction.crosshairUI then
			if not self.Interaction.InitCrosshair(self) then error("ClickableControls2: SetCrosshair failed to initialse crosshair") end
		end

		-- set both boxes and clickable indicator inactive for reliability
		self.Interaction.nameBox:SetActive(false)
		self.Interaction.infoBox:SetActive(false)
		self.Interaction.clickableIndicator:SetActive(false)

		if not self.Clickables.currentClickable then return end

		-- handle setting indicator on crosshair
		self.Interaction.clickableIndicator:SetActive(true)

		-- handle setting name on crosshair
		if self.Clickables.currentClickable.info.name then
			if #self.Clickables.currentClickable.info.name > 0 then
				self.Interaction.nameBox:SetActive(true)
				self.Interaction.nameText.text = self.Clickables.currentClickable.info.name
			end
		end

		-- handle setting infobox on crosshair
		self.Interaction.crosshairSetTime = nil
		if self.Clickables.currentClickable.info.tooltip then
			if #self.Clickables.currentClickable.info.tooltip > 0 then
				self.Interaction.crosshairSetTime = Time.time
			end
		end
	end,

	InitCrosshair = function()
		local self = ClickableControls2
		if not self then return end
		if self.Interaction.crosshairUI then return end -- crosshair loaded
		if not HBU.FileExists(self.resourceDir .. "/Assets/cc2_ui") then
			error("ClickableControls2: /Assets/cc2_ui does not exist!")
			return false
		end
		local bundle = AssetBundle.LoadFromFile(self.resourceDir .. "/Assets/cc2_ui")
		if not bundle then
			error("ClickableControls2: Could not load cc2_ui!")
			return false
		end
		local uiPrefab = bundle:LoadAsset("ClickableUI")
		if not uiPrefab then
			error("ClickableControls2: Could not load uiPrefab!")
			return false
		end

		-- create crosshair object
		self.Interaction.crosshairUI = GameObject.Instantiate(uiPrefab)
		bundle:Unload(false)
		self.Interaction.crosshair = self.Interaction.crosshairUI.transform:Find("Crosshair").gameObject;
		self.Interaction.clickableIndicator = self.Interaction.crosshair.transform:Find("ClickableIndicator").gameObject; 	self.Interaction.clickableIndicator:SetActive(false)
		self.Interaction.nameBox = self.Interaction.crosshair.transform:Find("Name").gameObject; 	self.Interaction.nameBox:SetActive(false)
		self.Interaction.nameText = self.Interaction.nameBox:GetComponentInChildren("UnityEngine.UI.Text")
		self.Interaction.infoBox = self.Interaction.crosshair.transform:Find("Info").gameObject;	self.Interaction.infoBox:SetActive(false)
		self.Interaction.infoText = self.Interaction.infoBox:GetComponentInChildren("UnityEngine.UI.Text")

		return true
	end,
}

-- Networking, does what it says on the tin
ClickableControls2.Networking = {

	--NetworkManager.connectors[0].myConnID
	NetworkManager = Slua.GetClass("HBNetworking.NetworkManager"),
	isConnected = false,
	myConnID = 0,

	lastBackgroundSync = 0, -- time last background sync
	lastPlayerActivity = 0, -- time player last clicked anything
	backgroundSyncVMS = nil, -- the last VMS we pulled clickables from to sync
	lastSyncedIndex = 1, -- the last clickable we synced from the VMS
	Update = function()
		local self = ClickableControls2
		if not self.Networking.NetworkManager then return end
		if not self.Networking.NetworkManager.isConnected then
			self.Networking.isConnected = false
			return
		end
		self.Networking.isConnected = true
		self.Networking.myConnID = self.Networking.NetworkManager.connectors[0].myConnID


		-- switch background sync rate according to usage
		local syncRate = self.Settings.networking.backgroundSyncRateSlow
		if Time.time < self.Networking.lastPlayerActivity + self.Settings.networking.playerActivityCooldown then
			syncRate = self.Settings.networking.backgroundSyncRateFast
		end

		-- check if we can sync next clickable yet
		if Time.time < self.Networking.lastBackgroundSync + syncRate then return end
		self.Networking.lastBackgroundSync = Time.time

		if Slua.IsNull(self.Networking.backgroundSyncVMS) or not self.Networking.backgroundSyncVMS then
			self.Networking.backgroundSyncVMS = self.Networking.GetNextVMS()
			if not self.Networking.backgroundSyncVMS then
				return
			end
			-- check if we should own any bases
			for base,v in pairs(self.Networking.backgroundSyncVMS.self) do
				if base.owner then
					self.Networking.ClaimOwner(base)
				end
			end
		end
		self.Networking.backgroundSyncVMS.self.lastSynced = Time.time

		if self.Networking.lastSyncedIndex > #self.Networking.backgroundSyncVMS.self.clickables then
			self.Networking.lastSyncedIndex = 1
			self.Networking.backgroundSyncVMS = nil
			return
		end

		for i=self.Networking.lastSyncedIndex, #self.Networking.backgroundSyncVMS.self.clickables do
			local clickable = self.Networking.backgroundSyncVMS.self.clickables[i]
			if clickable.networking then
				if clickable.networking.nb and clickable.networking.g then
					-- check if sync master is expired, if i own the clickable and sync master expired, claim it
					if clickable.networking.ownerBase.owner then
						if Time.time > self.Settings.networking.syncMasterCooldown + clickable.networking.lastSyncTime then
							if clickable.networking.syncMasterConnID ~= self.Networking.NetworkManager.connectors[0].myConnID then
								clickable.networking.syncMasterConnID = self.Networking.NetworkManager.connectors[0].myConnID
							end
						end
					end

					-- check sync master
					if clickable.networking.syncMasterConnID == self.Networking.NetworkManager.connectors[0].myConnID then
						self.Networking.lastSyncedIndex = i+1
						self.Networking.SendSync(clickable, true)
						return
					end
				end
			end
		end
	end,

	GetNextVMS = function()
		local self = ClickableControls2
		-- iterate VMS table and find VMS with most outdated last sync time
		-- if multiple VMS have the same last sync time (probably 0), sync the oldest VMS
		-- this ensures 'sync traps' like repeatedly spawning new vehicles don't cause other vehicles to stop syncing

		local vmsToSync = nil
		for k,vms in pairs(self.VMS.VMSTable) do
			if not vmsToSync then vmsToSync = vms; else -- got to start somewhere
				if vms.self.lastSynced < vmsToSync.self.lastSynced then
					-- this vms was synced longer ago
					 vmsToSync = vms
				elseif vms.self.lastSynced == vmsToSync.self.lastSynced then
					-- last synced at the same time, probably 0, choose oldest
					if vms.self.created < vmsToSync.self.created then
						-- this vms is older, sync this one
						vmsToSync = vms
					end
				end
			end
		end
		return vmsToSync
	end,

	lastEventTimes = {
		["down"] = 0, -- time last click event was synced
		["hold"] = 0, -- time last hold event was synced
		["up"] = 0, -- time last release event was synced
	},

	-- call made on click to register a sync event to queue and send
	SyncEvent = function(clickable, state)
		local self = ClickableControls2
		if not self.Networking.isConnected then return end

		if Slua.IsNull(clickable.button) or not clickable.button then return end
		if not clickable.networking then return end
		if not (clickable.networking.nb and clickable.networking.g) then return end
		if not clickable.networking.ownerBase.owner then
			-- check for otherPlayers state tag
			if not clickable.tags["otherPlayer"] then
				return
			end
			-- check if clickable will allow a new sync master
			if clickable.networking.syncMasterConnID ~= self.Networking.NetworkManager.connectors[0].myConnID then
				if Time.time < self.Settings.networking.syncMasterCooldown + clickable.networking.lastSyncTime then
					-- clickable not off cooldown
					return
				end
			end
		end

		-- set master sync to myself
		clickable.networking.syncMasterConnID = self.Networking.NetworkManager.connectors[0].myConnID
		clickable.networking.lastSyncTime = Time.time

		local rateLimit = self.Settings.networking.clickEventRateLimit
		if state == "hold" then
			rateLimit = self.Settings.networking.holdEventRateLimit
		end

		if Time.time > self.Networking.lastEventTimes[state] + rateLimit then
			-- can sync
			self.Networking.lastEventTimes[state] = Time.time
			self.Networking.SendSync(clickable)
			self.Networking.lastPlayerActivity = Time.time
		end
	end,

	-- send a network packet to synchronise a clickable
	SendSync = function(clickable, autoUpdate)
		local self = ClickableControls2
		if not self.Networking.isConnected then return end
		local dataArray = RhoNetLib.HBNetworking.HBNetUtils:CreateByteArray(4+1+clickable.button.outputValues.Count*4)

		-- include my connID and autoUpdate status for establishing sync priority
		RhoNetLib.HBNetworking.HBNetUtils:ConvertToBytes(dataArray, self.Networking.NetworkManager.connectors[0].myConnID, "int", 0)
		RhoNetLib.HBNetworking.HBNetUtils:ConvertToBytes(dataArray, autoUpdate or false, "bool", 4)
		for i=1, clickable.button.outputValues.Count do
			RhoNetLib.HBNetworking.HBNetUtils:ConvertToBytes(dataArray, clickable.button.outputValues[i-1], "float", ((i)*4)+1)
		end
		clickable.networking.g:GeneralSendData(dataArray)
	end,

	RecieveSync = function(clickable, syncData)
		local self = ClickableControls2
		if not self.Networking.isConnected then return end

		if clickable.networking.ownerBase.owner then
			-- check for otherPlayer state tag
			if not clickable.tags["otherPlayer"] then
				return -- nah mate fuck off
			end
		end

		local shouldSync = false
		local priority = false

		-- get user connID and sync type
		local syncConnID = RhoNetLib.HBNetworking.HBNetUtils:ConvertFromBytes(syncData.data, "int", 0)
		local wasAutoUpdate = RhoNetLib.HBNetworking.HBNetUtils:ConvertFromBytes(syncData.data, "bool", 4)

		-- owner sending a player sync update always takes priority
		if syncConnID == clickable.networking.ownerBase.connID and not wasAutoUpdate then
			priority = true
			shouldSync = true
		end

		-- wasn't an owner sync, establish whether sync is valid othersync
		if not priority then
			if clickable.networking.syncMasterConnID ~= syncConnID then
				-- sync request was not master
				-- check whether they can become the new sync master
				if Time.time > self.Settings.networking.syncMasterCooldown + clickable.networking.lastSyncTime then
					-- other player can become sync master
					clickable.networking.syncMasterConnID = syncConnID
					shouldSync = true
				end
			else -- sync request was master
				shouldSync = true -- sync appears valid
			end
		end

		if not shouldSync then return end -- sync does not appear valid, abort

		-- sync must be valid, update clickable sync params
		clickable.networking.syncMasterConnID = syncConnID
		if not wasAutoUpdate then
			-- if player event update, refresh master status
			clickable.networking.lastSyncTime = Time.time
		end

		-- deserialise sync data and apply to clickable
		for i=1, clickable.button.outputValues.Count do
			local index = ((i)*4)+1
			local syncVal = 0
			if not (index > syncData.data.Length) then
				syncVal = RhoNetLib.HBNetworking.HBNetUtils:ConvertFromBytes(syncData.data, "float", index)
			end
			clickable.button.outputValues[i-1] = syncVal
		end
		clickable.button:UpdateOutputs()
		self.Clickables.UpdateMovables(clickable)
	end,

	ClaimOwner = function(base)
		local self = ClickableControls2
		if not self.Networking.isConnected then return end

		if not base.owner then return end -- can't claim ownershp of a base we don't own

		local dataArray = RhoNetLib.HBNetworking.HBNetUtils:CreateByteArray(4)

		-- include my connID and autoUpdate status for establishing sync priority
		RhoNetLib.HBNetworking.HBNetUtils:ConvertToBytes(dataArray, self.Networking.NetworkManager.connectors[0].myConnID, "int", 0)
		base.gameObject:GetComponent("HBNetworking.General"):GeneralSendData(dataArray)
	end,

	RecieveOwner = function(base, syncData)
		local self = ClickableControls2
		if not self.Networking.isConnected then return end

		if base.owner then return end -- don't accept ownership claim of a base we own

		-- get connID of player who claims base
		local syncConnID = RhoNetLib.HBNetworking.HBNetUtils:ConvertFromBytes(syncData.data, "int", 0)

		-- set base ownership
		if base.connID ~= syncConnID then
			base.connID = syncConnID
		end
	end,
}

-- Clickables handles operation of actual clickable parts
ClickableControls2.Clickables = {
	-- clickableTypes are the clickable behaviours
	clickableTypes = {
		pushbutton = {
			displayName = "Pushbutton",
			params = {},
			func = function(clickable, button, state) -- on while held
				if not clickable then return end
				if not clickable.button then return end
				if not state then return end
				local self = ClickableControls2
				if not self then return end
				if not (button == "primary") then return end

				if not (state == "up") then
					clickable.button.outputValues[0] = 1
				else
					clickable.button.outputValues[0] = 0
				end

				-- for reliability
				for i=1, clickable.button.outputValues.Count do
					clickable.button.outputValues[i-1] = clickable.button.outputValues[0]
				end
				clickable.button:UpdateOutputs()
				self.Clickables.UpdateMovables(clickable)
			end,
		},

		momentary = {
			displayName = "Momentary",
			params = {
				{
					["name"] = "duration",
					["displayName"] = "Momentary Duration",
					["tooltip"] = "How long this clickable stays on for after clicking",
					["uiType"] = "floatProperty",
					["minValue"] = -1000,
					["maxValue"] = 0
				},
			},
			func = function(clickable, button, state) -- on for set duration, if no duration set then on for 1 frame
				if not clickable then return end
				if not clickable.button then return end
				if not state then return end
				local self = ClickableControls2
				if not self then return end
				if not (button == "primary") then return end

				if (state == "down") then
					clickable.button.outputValues[0] = 1
					self.Clickables.clickedTime = Time.time
				else
					if clickable.params.duration and not (state == "up") then
						if Time.time > self.Clickables.clickedTime + tonumber(clickable.params.duration) then
							clickable.button.outputValues[0] = 0
							self.Clickables.clickedTime = nil
						end
					else
						clickable.button.outputValues[0] = 0
						self.Clickables.clickedTime = nil
					end
				end

				-- for reliability
				for i=1, clickable.button.outputValues.Count do
					clickable.button.outputValues[i-1] = clickable.button.outputValues[0]
				end
				clickable.button:UpdateOutputs()
				self.Clickables.UpdateMovables(clickable)
			end,
		},

		toggle = {
			displayName = "Toggle",
			params = {},
			func = function(clickable, button, state) -- toggles on or off
				if not clickable then return end
				if not clickable.button then return end
				if not state then return end
				local self = ClickableControls2
				if not self then return end
				if not (state == "down") then return end
				if not (button == "primary") then return end
				if clickable.button.outputValues[0] > 0 then
					clickable.button.outputValues[0] = 0
				else
					clickable.button.outputValues[0] = 1
				end
				-- for reliability
				for i=1, clickable.button.outputValues.Count do
					clickable.button.outputValues[i-1] = clickable.button.outputValues[0]
				end
				clickable.button:UpdateOutputs()
				self.Clickables.UpdateMovables(clickable)
			end,
		},

		missiletoggle = {
			displayName = "Missile Toggle",
			params = {},
			func = function(clickable, button, state) -- toggle with a cover. cover is toggled with secondary, switch can only be toggled on if cover open, closing cover toggles off switch
				if not clickable then return end
				if not clickable.button then return end
				if not state then return end
				local self = ClickableControls2
				if not self then return end
				if not (state == "down") then return end

				if (button == "secondary") then
					if clickable.button.outputValues[1] > 0 then
						clickable.button.outputValues[1] = 0
						clickable.button.outputValues[0] = 0
					else
						clickable.button.outputValues[1] = 1
					end
				else
					if clickable.button.outputValues[0] > 0 then
						clickable.button.outputValues[0] = 0
					else
						clickable.button.outputValues[0] = clickable.button.outputValues[1]
					end
				end

				-- for reliability, always forces switch to 0 if cover is 0
				clickable.button.outputValues[0] = math.min(clickable.button.outputValues[1], clickable.button.outputValues[0])

				clickable.button:UpdateOutputs()
				self.Clickables.UpdateMovables(clickable)
			end,
		},

		incremental = {
			displayName = "Incremental",
			params = {
				{
					["name"] = "min",
					["displayName"] = "Min Value",
					["tooltip"] = "The minimum output value this clickable can reach",
					["uiType"] = "intProperty",
					["minValue"] = -1000,
					["maxValue"] = 0
				},
				{
					["name"] = "max",
					["displayName"] = "Max Value",
					["tooltip"] = "The maximum output value this clickable can reach",
					["uiType"] = "intProperty",
					["minValue"] = 0,
					["maxValue"] = 1000
				},
			},
			func = function(clickable, button, state) -- left click to increment by 1, right click to decrement by 1
				if not clickable then return end
				if not clickable.button then return end
				if not button then return end
				if not state then return end
				local self = ClickableControls2
				if not self then return end
				if not (state == "down") then return end

				if button == "primary" then -- increment
					if clickable.params.max then
						if tonumber(clickable.params.max) > clickable.button.outputValues[0] then
							clickable.button.outputValues[0] = clickable.button.outputValues[0] + 1
						end
					end
				elseif button == "secondary" then -- decrement
					if clickable.params.min then
						if tonumber(clickable.params.min) < clickable.button.outputValues[0] then
							clickable.button.outputValues[0] = clickable.button.outputValues[0] - 1
						end
					end
				end

				-- for reliability
				for i=1, clickable.button.outputValues.Count do
					clickable.button.outputValues[i-1] = clickable.button.outputValues[0]
				end
				clickable.button:UpdateOutputs()
				self.Clickables.UpdateMovables(clickable)
			end,
		},

		incrementalDecimal = {
			displayName = "Decimal Incremental",
			params = {
				{
					["name"] = "min",
					["displayName"] = "Min Value",
					["tooltip"] = "The minimum output value this clickable can reach",
					["uiType"] = "floatProperty",
					["minValue"] = -1000,
					["maxValue"] = 0
				},
				{
					["name"] = "max",
					["displayName"] = "Max Value",
					["tooltip"] = "The maximum output value this clickable can reach",
					["uiType"] = "floatProperty",
					["minValue"] = 0,
					["maxValue"] = 1000
				},
				{
					["name"] = "speed",
					["displayName"] = "Speed",
					["tooltip"] = "The speed at which this clickable increments/decrements",
					["uiType"] = "floatProperty",
					["minValue"] = -1000,
					["maxValue"] = 1000
				},
			},
			func = function(clickable, button, state) -- left click to smooth increment, right click to smooth decrement
				if not clickable then return end
				if not clickable.button then return end
				if not button then return end
				local self = ClickableControls2
				if not self then return end
				local speed = clickable.params.speed
				if not speed then speed = 1 end
				local stepValue = speed * Time.deltaTime
				if button == "primary" then -- increment
					if clickable.params.max then
						if tonumber(clickable.params.max) > clickable.button.outputValues[0] then
							clickable.button.outputValues[0] = clickable.button.outputValues[0] + stepValue
						end
					end
				elseif button == "secondary" then -- decrement
					if clickable.params.min then
						if tonumber(clickable.params.min) < clickable.button.outputValues[0] then
							clickable.button.outputValues[0] = clickable.button.outputValues[0] - stepValue
						end
					end
				end
				if clickable.button.outputValues[0] > tonumber(clickable.params.max) then
					clickable.button.outputValues[0] = tonumber(clickable.params.max)
				elseif clickable.button.outputValues[0] < tonumber(clickable.params.min) then
					clickable.button.outputValues[0] = tonumber(clickable.params.min)
				end
				-- for reliability
				for i=1, clickable.button.outputValues.Count do
					clickable.button.outputValues[i-1] = clickable.button.outputValues[0]
				end
				clickable.button:UpdateOutputs()
				self.Clickables.UpdateMovables(clickable)
			end,
		},

		draggablex = {
			displayName = "One Axis Draggable",
			params = {
				{
					["name"] = "min",
					["displayName"] = "Min Value",
					["tooltip"] = "The minimum output value this clickable can reach",
					["uiType"] = "floatProperty",
					["minValue"] = -1000,
					["maxValue"] = 0
				},
				{
					["name"] = "max",
					["displayName"] = "Max Value",
					["tooltip"] = "The maximum output value this clickable can reach",
					["uiType"] = "floatProperty",
					["minValue"] = 0,
					["maxValue"] = 1000
				},
			},
			func = function(clickable, button, state) -- sets output to X position on hitbox, allows value to be 'dragged' by cursor
				if not clickable then return end
				if not clickable.button then return end
				if not button then return end
				if not (button == "primary") then return end
				local self = ClickableControls2
				if not self then return end
				if not self.Clickables.lastHitinfo then return end
				local point = self.Clickables.lastHitinfo.collider.transform:InverseTransformPoint(self.Clickables.lastHitinfo.point)
				local pointVal = math.min(math.max(point.x,tonumber(clickable.params.hitmin)),point.x,tonumber(clickable.params.hitmax)) -- clamp
				local lerpPoint = self.InverseLerp(tonumber(clickable.params.hitmin), tonumber(clickable.params.hitmax), pointVal) -- to get 0-1 value
				local outVal = self.Lerp(tonumber(clickable.params.min), tonumber(clickable.params.max), lerpPoint) -- real value
				clickable.button.outputValues[0] = outVal
				-- for reliability
				for i=1, clickable.button.outputValues.Count do
					clickable.button.outputValues[i-1] = clickable.button.outputValues[0]
				end
				clickable.button:UpdateOutputs()
				self.Clickables.UpdateMovables(clickable)
			end,
		},

		draggablexcentering = {
			displayName = "One Axis Draggable Centering",
			params = {
				{
					["name"] = "min",
					["displayName"] = "Min Value",
					["tooltip"] = "The minimum output value this clickable can reach",
					["uiType"] = "floatProperty",
					["minValue"] = -1000,
					["maxValue"] = 0
				},
				{
					["name"] = "max",
					["displayName"] = "Max Value",
					["tooltip"] = "The maximum output value this clickable can reach",
					["uiType"] = "floatProperty",
					["minValue"] = 0,
					["maxValue"] = 1000
				},
			},
			func = function(clickable, button, state) -- same as draggable but auto centering
				if not clickable then return end
				if not clickable.button then return end
				if not button then return end
				if not (button == "primary") then return end
				local self = ClickableControls2
				if not self then return end
				local outVal = 0
				if not (state == "up") then
					if not self.Clickables.lastHitinfo then return end
					local point = self.Clickables.lastHitinfo.collider.transform:InverseTransformPoint(self.Clickables.lastHitinfo.point)
					local pointVal = math.min(math.max(point.x,tonumber(clickable.params.hitmin)),point.x,tonumber(clickable.params.hitmax)) -- clamp
					local lerpPoint = self.InverseLerp(tonumber(clickable.params.hitmin), tonumber(clickable.params.hitmax), pointVal) -- to get 0-1 value
					outVal = self.Lerp(tonumber(clickable.params.min), tonumber(clickable.params.max), lerpPoint) -- real value
				end
				clickable.button.outputValues[0] = outVal

				-- for reliability
				for i=1, clickable.button.outputValues.Count do
					clickable.button.outputValues[i-1] = clickable.button.outputValues[0]
				end
				clickable.button:UpdateOutputs()
				self.Clickables.UpdateMovables(clickable)
			end,
		},

		draggablexz = {
			displayName = "Two Axis Draggable",
			params = {
				{
					["name"] = "min",
					["displayName"] = "Min Value",
					["tooltip"] = "The minimum output value this clickable can reach",
					["uiType"] = "floatProperty",
					["minValue"] = -1000,
					["maxValue"] = 0
				},
				{
					["name"] = "max",
					["displayName"] = "Max Value",
					["tooltip"] = "The maximum output value this clickable can reach",
					["uiType"] = "floatProperty",
					["minValue"] = 0,
					["maxValue"] = 1000
				},
			},
			func = function(clickable, button, state) -- sets output to X position on hitbox, allows value to be 'dragged' by cursor
				if not clickable then return end
				if not clickable.button then return end
				if not button then return end
				local self = ClickableControls2
				if not self then return end
				if not self.Clickables.lastHitinfo then return end
				local outValX = 0
				local outValZ = 0

				if (button == "primary") then
					local point = self.Clickables.lastHitinfo.collider.transform:InverseTransformPoint(self.Clickables.lastHitinfo.point)

					-- X point
					local pointValX = math.min(math.max(point.x,tonumber(clickable.params.hitmin)),point.x,tonumber(clickable.params.hitmax)) -- clamp
					local lerpPointX = self.InverseLerp(tonumber(clickable.params.hitmin), tonumber(clickable.params.hitmax), pointValX) -- to get 0-1 value
					outValX = self.Lerp(tonumber(clickable.params.min), tonumber(clickable.params.max), lerpPointX) -- real value

					-- Y point
					local pointValZ = math.min(math.max(point.z,tonumber(clickable.params.hitmin)),point.z,tonumber(clickable.params.hitmax)) -- clamp
					local lerpPointZ = self.InverseLerp(tonumber(clickable.params.hitmin), tonumber(clickable.params.hitmax), pointValZ) -- to get 0-1 value
					outValZ = self.Lerp(tonumber(clickable.params.min), tonumber(clickable.params.max), lerpPointZ) -- real value
				end

				clickable.button.outputValues[0] = outValX
				clickable.button.outputValues[1] = outValZ

				clickable.button:UpdateOutputs()
				self.Clickables.UpdateMovables(clickable)
			end,
		},

		draggablexzcentering = {
			displayName = "Two Axis Draggable Centering",
			params = {
				{
					["name"] = "min",
					["displayName"] = "Min Value",
					["tooltip"] = "The minimum output value this clickable can reach",
					["uiType"] = "floatProperty",
					["minValue"] = -1000,
					["maxValue"] = 0
				},
				{
					["name"] = "max",
					["displayName"] = "Max Value",
					["tooltip"] = "The maximum output value this clickable can reach",
					["uiType"] = "floatProperty",
					["minValue"] = 0,
					["maxValue"] = 1000
				},
			},
			func = function(clickable, button, state) -- sets output to X position on hitbox, allows value to be 'dragged' by cursor
				if not clickable then return end
				if not clickable.button then return end
				if not button then return end
				if not (button == "primary") then return end
				local self = ClickableControls2
				if not self then return end
				local outValX = 0
				local outValZ = 0
				if not (state == "up") then
					if not self.Clickables.lastHitinfo then return end
					local point = self.Clickables.lastHitinfo.collider.transform:InverseTransformPoint(self.Clickables.lastHitinfo.point)
					-- X point
					local pointValX = math.min(math.max(point.x,tonumber(clickable.params.hitmin)),point.x,tonumber(clickable.params.hitmax)) -- clamp
					local lerpPointX = self.InverseLerp(tonumber(clickable.params.hitmin), tonumber(clickable.params.hitmax), pointValX) -- to get 0-1 value
					outValX = self.Lerp(tonumber(clickable.params.min), tonumber(clickable.params.max), lerpPointX) -- real value

					-- Y point
					local pointValZ = math.min(math.max(point.z,tonumber(clickable.params.hitmin)),point.z,tonumber(clickable.params.hitmax)) -- clamp
					local lerpPointZ = self.InverseLerp(tonumber(clickable.params.hitmin), tonumber(clickable.params.hitmax), pointValZ) -- to get 0-1 value
					outValZ = self.Lerp(tonumber(clickable.params.min), tonumber(clickable.params.max), lerpPointZ) -- real value
				end

				clickable.button.outputValues[0] = outValX
				clickable.button.outputValues[1] = outValZ

				clickable.button:UpdateOutputs()
				self.Clickables.UpdateMovables(clickable)
			end,
		},

		draggableangle = {
			displayName = "Angular Draggable",
			params = {
				{
					["name"] = "min",
					["displayName"] = "Min Value",
					["tooltip"] = "The minimum output value this clickable can reach",
					["uiType"] = "floatProperty",
					["minValue"] = -1000,
					["maxValue"] = 0
				},
				{
					["name"] = "max",
					["displayName"] = "Max Value",
					["tooltip"] = "The maximum output value this clickable can reach",
					["uiType"] = "floatProperty",
					["minValue"] = 0,
					["maxValue"] = 1000
				},
				{
					["name"] = "anglemin",
					["displayName"] = "Min Angle",
					["tooltip"] = "The minimum angle limit of this clickable",
					["uiType"] = "floatProperty",
					["minValue"] = -1000,
					["maxValue"] = 0
				},
				{
					["name"] = "anglemax",
					["displayName"] = "Max Angle",
					["tooltip"] = "The maximum angle limit of this clickable",
					["uiType"] = "floatProperty",
					["minValue"] = 0,
					["maxValue"] = 1000
				},
			},
			func = function(clickable, button, state) -- sets output to angle from centre of hitbox, allows value to be 'dragged' by cursor
				if not clickable then return end
				if not clickable.button then return end
				if not button then return end
				local self = ClickableControls2
				if not self then return end
				if not self.Clickables.lastHitinfo then return end
				outVal = 0
				if (button == "primary") then
					local point = self.Clickables.lastHitinfo.collider.transform:InverseTransformPoint(self.Clickables.lastHitinfo.point)
					point = Vector2(point.x,point.z)
					local angle = Vector2.Angle(Vector2.up, point)
					local sign = Mathf.Sign(Vector2.Dot(Vector2.right, point))
					if sign < 0 then angle = 360 - angle end
					-- smart clamp according to delta angle and threshold, so 0 limited angles don't loop over
					if angle < tonumber(clickable.params.anglemin) or angle > tonumber(clickable.params.anglemax) then
						if math.abs(Mathf.DeltaAngle(tonumber(clickable.params.anglemin), angle)) < 10 then
							angle = tonumber(clickable.params.anglemin)
						elseif math.abs(Mathf.DeltaAngle(tonumber(clickable.params.anglemax), angle)) < 10 then
							angle = tonumber(clickable.params.anglemax)
						else
							return
						end
					end
					local angleVal = math.min(math.max(angle,tonumber(clickable.params.anglemin)),tonumber(clickable.params.anglemax)) -- clamp
					local lerpangle = self.InverseLerp(tonumber(clickable.params.anglemin), tonumber(clickable.params.anglemax), angleVal) -- to get 0-1 value
					outVal = self.Lerp(tonumber(clickable.params.min), tonumber(clickable.params.max), lerpangle) -- real value
				end
				clickable.button.outputValues[0] = outVal
				clickable.button:UpdateOutputs()
				self.Clickables.UpdateMovables(clickable)
			end,
		},

		ignitionkey = {
			displayName = "Ignition Key",
			params = {},
			func = function(clickable, button, state) -- 4 outputs, pos, acc, ign, start. incremental until start, which is held
				if not clickable then return end
				if not clickable.button then return end
				if not button then return end
				local self = ClickableControls2
				if not self then return end

				if button == "primary" then
					if state == "down" then
						if clickable.button.outputValues[0] < 3 then -- increment
							clickable.button.outputValues[0] = clickable.button.outputValues[0] + 1
						end
					elseif state == "up" then
						if clickable.button.outputValues[0] > 2 then -- release start
							clickable.button.outputValues[0] = 2
						end
					end
				elseif button == "secondary" then -- decrement
					if state == "down" then
						if clickable.button.outputValues[0] > 0 then
							clickable.button.outputValues[0] = clickable.button.outputValues[0] - 1
						end
					end
				end

				-- set other wires
				clickable.button.outputValues[1] = math.min(clickable.button.outputValues[0], 1) -- acc
				clickable.button.outputValues[2] = math.max(0, math.min(clickable.button.outputValues[0]-1, 1)) -- ign
				clickable.button.outputValues[3] = math.max(0, math.min(clickable.button.outputValues[0]-2, 1)) -- start

				clickable.button:UpdateOutputs()
				self.Clickables.UpdateMovables(clickable)
			end,
		},

		------------------------------------------------------
		-- keypads and keyboards
		------------------------------------------------------
		-- keys are defined on moveables
		-- clickable area is defined by corner vector2s
		-- c1x, c1z, c2x, c2z
		-- key value is defined by val

		numpadMomentary = {
			displayName = "Numpad Momentary",
			params = {},
			func = function(clickable, button, state) -- outputs the current numpad value pressed
				if not clickable then return end
				if not clickable.button then return end
				if not clickable.params.subClickables then return end
				if not button then return end
				local self = ClickableControls2
				if not self then return end
				local value = 0

				if (button == "primary" and state == "down") then
					local key = -1 -- key is index of key resolved, -1 means not resolved

					local point = self.Clickables.lastHitinfo.collider.transform:InverseTransformPoint(self.Clickables.lastHitinfo.point)
					point = Vector2(point.x, point.z)

					key = self.Clickables.SubClickables.ResolveSubClickable(clickable, point)
					if key == -1 then return end -- no key resolved, probably not over a key

					-- key to output
					if clickable.params.subClickables[key].val ~= "del" then
						value = tonumber(clickable.params.subClickables[key].val)
					else
						value = -1 -- del should output -1
					end

					-- set output and mover
					clickable.button.outputValues[0] = value
					clickable.button:UpdateOutputs()
					self.Clickables.UpdateMovables(clickable, key, 1)
					return
				elseif (state == "up") then
					-- reset
					clickable.button.outputValues[0] = value
					clickable.button:UpdateOutputs()
					self.Clickables.UpdateMovables(clickable)
					return
				end

			end,
			init = function(clickable) -- one time init script for clickable startup
				local self = ClickableControls2
				clickable.params.subClickables = self.Clickables.SubClickables.GetSubClickables(clickable)
			end,
		},


		-- unfinished, see issue #25
		numpadSequence = {
			displayName = "Numpad Sequence",
			params = {
				{
					["name"] = "min",
					["displayName"] = "Min Value",
					["tooltip"] = "The minimum output value this clickable can reach",
					["uiType"] = "floatProperty",
					["minValue"] = -1000,
					["maxValue"] = 0
				},
				{
					["name"] = "max",
					["displayName"] = "Max Value",
					["tooltip"] = "The maximum output value this clickable can reach",
					["uiType"] = "floatProperty",
					["minValue"] = 0,
					["maxValue"] = 1000
				},
				{
					["name"] = "useInt",
					["displayName"] = "Use Integer",
					["tooltip"] = "Toggle between outputting Integer (whole number only) or decimal numbers",
					["uiType"] = "boolProperty",
					-- ["minValue"] = 0,
					-- ["maxValue"] = 1000
				},
			},
			func = function(clickable, button, state) -- outputs the current numpad value pressed
				if not clickable then return end
				if not clickable.button then return end
				if not clickable.params.subClickables then return end
				if not button then return end
				local self = ClickableControls2
				if not self then return end
				local value = 0

				-- if primary, then numpad works normally, left clicking del acts as a period, right clicking del acts as backspace
				-- if backspacing a string with 1 length, set string to "0"


				if (button == "primary" and state == "down") then
					local key = -1 -- key is index of key resolved, -1 means not resolved

					local point = self.Clickables.lastHitinfo.collider.transform:InverseTransformPoint(self.Clickables.lastHitinfo.point)
					point = Vector2(point.x, point.z)

					key = self.Clickables.SubClickables.ResolveSubClickable(clickable, point)
					if key == -1 then return end -- no key resolved, probably not over a key

					-- key to output
					if clickable.params.subClickables[key].val ~= "del" then
						value = tonumber(clickable.params.subClickables[key].val)
					else
						value = -1 -- del should output -1
					end

					-- set output and mover
					clickable.button.outputValues[0] = value
					clickable.button:UpdateOutputs()
					self.Clickables.UpdateMovables(clickable, key, 1)
					return
				elseif (state == "up") then
					-- reset
					clickable.button.outputValues[0] = value
					clickable.button:UpdateOutputs()
					self.Clickables.UpdateMovables(clickable)
					return
				end

			end,
			init = function(clickable) -- one time init script for clickable startup
				local self = ClickableControls2
				clickable.params.subClickables = self.Clickables.SubClickables.GetSubClickables(clickable)
				clickable.params.currentVal = "0"
			end,
		},


	},
	-- moveableTypes are the ways clickables can animate themselves
	moveableTypes = {
		rotate = {
			displayName = "Rotatable",
			params = {
				{
					name = "min",
					displayName = "Angle Min",
					tooltip = "The minimum angle this moveable part can reach",
					uiType = "floatProperty",
					minValue = -3600,
					maxValue = 3600,
				},
				{
					name = "max",
					displayName = "Angle Max",
					tooltip = "The maximum angle this moveable part can reach",
					uiType = "floatProperty",
					minValue = -3600,
					maxValue = 3600,
				},
			},
			func = function(moveable, state)
				if not moveable then return end
				if not state then return end
				local self = ClickableControls2
				if not self then return end
				local newAngle = Vector3.zero
				-- lerp between min and max
				newAngle[moveable.axis] = self.Lerp(tonumber(moveable.min), tonumber(moveable.max), state)
				moveable.moveable.transform.localEulerAngles = newAngle
			end,
		},

		translate = {
			displayName = "Translatable",
			params = {
				{
					name = "min",
					displayName = "Translate Min",
					tooltip = "The minimum local translation this moveable part can reach",
					uiType = "floatProperty",
					minValue = -1000,
					maxValue = 1000,
				},
				{
					name = "max",
					displayName = "Translate Max",
					tooltip = "The maximum local translation this moveable part can reach",
					uiType = "floatProperty",
					minValue = -1000,
					maxValue = 1000,
				},
			},
			func = function(moveable, state)
				if not moveable then return end
				if not state then return end
				local self = ClickableControls2
				if not self then return end
				local newPos = moveable.moveable.transform.localPosition
				-- lerp between min and max
				newPos[moveable.axis] = self.Lerp(tonumber(moveable.min), tonumber(moveable.max), state)
				moveable.moveable.transform.localPosition = newPos
			end,
		},

		activate = {
			displayName = "Activatable",
			params = {
				-- {
				-- 	name = "min",
				-- 	displayName = "Deactivate",
				-- 	tooltip = "The minimum local translation this moveable part can reach",
				-- 	uiType = "floatProperty",
				-- 	minValue = -1000,
				-- 	maxValue = 1000,
				-- },
				-- {
				-- 	name = "max",
				-- 	displayName = "Activate",
				-- 	tooltip = "The maximum local translation this moveable part can reach",
				-- 	uiType = "floatProperty",
				-- 	minValue = -1000,
				-- 	maxValue = 1000,
				-- },
			},
			func = function(moveable, state)
				if not moveable then return end
				if not state then return end
				local self = ClickableControls2
				if not self then return end
				-- local newPos = moveable.moveable.transform.localPosition
				-- lerp between min and max
				moveable.moveable.gameObject:SetActive(toboolean(state))
				-- newPos[moveable.axis] = self.Lerp(tonumber(moveable.min), tonumber(moveable.max), state)
				-- moveable.moveable.transform.localPosition = newPos
			end,
		},
	},

	UpdateMovables = function(clickable, mover, val)
		local self = ClickableControls2
		if not clickable then return end
		if not clickable.moveables or not (#clickable.moveables > 0) then return end
		if mover then
			local moveable = clickable.moveables[mover]
			local val2 = val or clickable.button.outputValues[mover-1]
			if self.Clickables.moveableTypes[moveable.type] then
				self.Clickables.moveableTypes[moveable.type].func(moveable, val2)
			end
			return
		end
		for i=1, #clickable.moveables do
			local moveable = clickable.moveables[i]
			local val2 = nil
			if not val then val2 = self.InverseLerp(tonumber(clickable.params.min) or 0, tonumber(clickable.params.max) or 1, clickable.button.outputValues[math.min(i, clickable.button.outputValues.Count)-1]) end
			if self.Clickables.moveableTypes[moveable.type] then
				self.Clickables.moveableTypes[moveable.type].func(moveable, val2)
			end
		end
	end,

	-- SubClickables namespace is for functions and data enabling a single collider to have multiple clickable zones, like a keyboard
	SubClickables = {
		lastSubClickableIndex = -1,
		lastSubClickablePoint = Vector2.zero,
		GetSubClickables = function(clickable) -- finds all sub clickables on clickable, TODO fix this so subclickables don't need to be movers
			local subClickables = {}
			for moveable in Slua.iter(clickable.button.moveables) do
				local sDataComp = moveable.gameObject:GetComponent("HBBuilder.StringDataContainer")
				local c1x = tonumber(HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, "c1x"))
				local c1z = tonumber(HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, "c1z"))
				local c2x = tonumber(HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, "c2x"))
				local c2z = tonumber(HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, "c2z"))
				local val = HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, "val")

				if c1x and c1z and c2x and c2z and val then
					subClickables[#subClickables+1] = {
						["c1"] = Vector2(c1x, c1z),
						["c2"] = Vector2(c2x, c2z),
						["val"] = val
					}
				end
			end

			return subClickables
		end,
		ResolveSubClickable = function(clickable, point) -- takes a vector2, resolves this to known subClickables
			local self = ClickableControls2
			-- cache stuff
			if self.Clickables.SubClickables.lastSubClickableIndex ~= -1 then
				-- check if we're still looking at the same point
				if point == self.Clickables.SubClickables.lastSubClickablePoint then
					return self.Clickables.SubClickables.lastSubClickableIndex
				end
				-- check if we're still over the last hit subClickable
				local lastSubClickable = clickable.params.subClickables[self.Clickables.SubClickables.lastSubClickableIndex]
				if point.x > lastSubClickable.c1.x and point.x < lastSubClickable.c2.x and point.y > lastSubClickable.c1.y and point.y < lastSubClickable.c2.y then
					-- cache new point
					self.Clickables.SubClickables.lastSubClickablePoint = point
					return self.Clickables.SubClickables.lastSubClickableIndex
				end
				-- cache miss, solve for new one
				self.Clickables.SubClickables.lastSubClickableIndex = -1
			end

			-- resolve subClickable
			for i=1,#clickable.params.subClickables do
				-- lazy evaluate... looks ugly, runs fast
				if point.x > clickable.params.subClickables[i].c1.x then
					if point.x < clickable.params.subClickables[i].c2.x then
						if point.y > clickable.params.subClickables[i].c1.y then
							if point.y < clickable.params.subClickables[i].c2.y then
								self.Clickables.SubClickables.lastSubClickableIndex = i
								return i -- we're over this subClickable
							end
						end
					end
				end
			end

			return -1 -- no subClickable found
		end,
	},

	CheckLastClickState = function() -- verify that last clickable recieved up state correctly for stability
		local self = ClickableControls2
		if not self.Clickables.lastClicked then return end
		if self.Clickables.currentClickable then
			if self.Clickables.lastClicked.clickable.button == self.Clickables.currentClickable.button then return end
		end
		if self.Clickables.lastClicked.state ~= "up" then
			local clickable = self.Clickables.lastClicked.clickable
			local button = self.Clickables.lastClicked.button
			self.Clickables.lastClicked = nil
			self.Clickables.OnClick(clickable, button, "up")
		end
		self.Clickables.lastClicked = nil
	end,

	Clear = function()
		local self = ClickableControls2
		self.Clickables.currentClickable = nil
		self.Clickables.lastHitinfo = nil
		self.Clickables.SubClickables.lastSubClickableIndex = -1
		self.Clickables.SubClickables.lastSubClickablePoint = Vector2.zero
	end,

	Raycast = function() -- raycast for a clickable
		local self = ClickableControls2
		local layerMask = 64
		local direction = Camera.main.transform.forward
		if self.Interaction.clickMode == "free" then
			local pcCursor = self.PlayerController.cursor

			-- calculate world point from camera, ScreenPointToRay is a frame behind and causes jitter
			local fov2 = Camera.main.fieldOfView/2
			local aspect = Camera.main.aspect
			local point = math.sin(fov2*0.01745) / math.sin((90-fov2)*0.01745) * 2
			local xCoef = point * aspect
			local yCoef = point

			local pointPos = Vector3((pcCursor.Position.x/Screen.width-0.5)*xCoef, ((pcCursor.Position.y*-1+Screen.height)/Screen.height-0.5)*yCoef, 1)
			direction = Camera.main.transform:TransformPoint(pointPos) - Camera.main.transform.position
		end
		local ray, hitinfo = Physics.Raycast(Camera.main.transform.position, direction, Slua.out, self.Settings.clickRange, layerMask)
		if(ray) then
			-- optimisation check. will bail if we're raycasting on the same clickable, chances are we'll raycast to the same clickable a lot, since this runs in fixedUpdate
			if self.Clickables.lastHitinfo then
				if self.Clickables.lastHitinfo.collider == hitinfo.collider then
					self.Clickables.lastHitinfo = hitinfo -- still update last hit info though, so that hitpoint updates
					return
				end
			end
			local currentClickable = self.Clickables.GetClickable(hitinfo.collider)
			if currentClickable then
				self.Clickables.currentClickable = currentClickable  -- what we're currently looking at, if we're looking at anything
				self.Interaction.SetCrosshair()
			end
			self.Clickables.lastHitinfo = hitinfo -- save last hitinfo here, so we can get things like the hit coordinate etc, and do some optimisations
		else
			if self.Clickables.currentClickable then
				self.Clickables.Clear()
				self.Interaction.SetCrosshair()
			end
		end
	end,

	OnClick = function(clickable, button, state)
		if not clickable then return end
		if not button then button = "primary" end
		if not state then state = "down" end
		local self = ClickableControls2
		if not self then return end
		if not self.Clickables.clickableTypes[clickable.params.type] then return end

		-- check whether we can use this clickable
		if self.Networking.isConnected and clickable.networking then
			if not clickable.networking.ownerBase.owner then
				-- check for otherPlayers state tag
				if not clickable.tags["otherPlayer"] then
					return
				end
				-- check whether we can become the sync master
				if clickable.networking.syncMasterConnID ~= self.Networking.NetworkManager.connectors[0].myConnID then
					if Time.time < self.Settings.networking.syncMasterCooldown + clickable.networking.lastSyncTime then
						return -- clickable not off cooldown
					end
				end
			end
		end

		self.Clickables.clickableTypes[clickable.params.type].func(clickable, button, state)
		self.Clickables.lastClicked = {clickable = clickable, button = button, state = state} -- store last clicked clickable in order to go back and force "up" state if need be

		-- net sync call
		if not self.Networking.isConnected then return end
		self.Networking.SyncEvent(clickable, state)
	end,

	-- tries to get the clickable data table, from the VMS if it exists, or one-shot build if no VMS was found
	GetClickable = function(collider)
		if not collider then return end
		local self = ClickableControls2
		if not self then return end
		-- are we in the builder? the builder requires a custom data table
		if HBU.InBuilder() then
			if not ClickableControls2BuilderLua then return end
			return ClickableControls2BuilderLua:BuildClickableData(collider:GetComponentInParent("HBButton"))
		end
		-- does the parent have a VMS?
		local VMS =  self.VMS.GetVMSInParent(collider)
		if not VMS then return end
		local clickables = VMS.self.clickables
		for i=1,#clickables do
			local clickable = clickables[i]
			if clickable.hitbox == collider then -- got hit from clickables table
				return clickable
			end
		end
		-- TODO: implement one-shot data table building, for things like world clickables
	end,

	RegisterClick = function(clickable, button, state)
		if not (clickable and button and state) then return end
		local self = ClickableControls2.Clickables
		if self.queuedClick then return end
		self.queuedClick = {}
		self.queuedClick.clickable = clickable
		self.queuedClick.button = button
		self.queuedClick.state = state
	end,
}

-- VMS (vehicle management script) handles finding clickables on new vehicles, sorting them to an array and storing them, setting their state, etc
ClickableControls2.VMS = {
	VMSTable = {},
	RegisterVMS = function(VMS) -- called when a VMS initialises, this adds the VMS to the masterlist
		if not VMS then return end
		local self = ClickableControls2
		if not self then return end
		if VMS[1] and VMS[2] then
			self.VMS.VMSTable[tostring(VMS[1])] = VMS[2]
		end
	end,
	UnRegisterVMS = function(VMSID) -- called when a VMS is destroyed, this removes the VMS from the masterlist
		if not VMSID then return end
		local self = ClickableControls2
		if not self then return end
		if self.VMS.VMSTable[tostring(VMSID)] then
			self.VMS.VMSTable[tostring(VMSID)] = nil
			self.VMS.CleanupVMSTable()
		end
	end,
	CleanupVMSTable = function() -- purges broken VMS registrations from the masterlist
		local self = ClickableControls2
		if not self then return end
		if not self.VMS.VMSTable then return end
		if #iter(self.VMS.VMSTable) == 0 then return end
		for VMS in pairs(self.VMS.VMSTable) do
			if Slua.IsNull(self.VMS.VMSTable[VMS]) then
				self.VMS.VMSTable[VMS] = nil
			end
		end
	end,
	VMSStatusRefresh = function()
		local self = ClickableControls2
		if not self then return end
		local needsCleanup = false
		for VMS in pairs(self.VMS.VMSTable) do
			if not Slua.IsNull(self.VMS.VMSTable[VMS]) then
				self.VMS.VMSTable[VMS].self:UpdateClickableStatus()
			else -- a VMS was null, need to cleanup VMS table
				needsCleanup = true
			end
		end
		if needsCleanup then
			self.VMS.CleanupVMSTable()
		end
	end,
	lastVMSRoot = nil, -- when we hit a VMS, store the root VMS part here, most of our checks will happen against the same vehicle
	lastVMS = nil, -- store the last hit VMS here, most frames will return the same one, so we can simplify the check
	GetVMSInParent = function(obj)
		local self = ClickableControls2
		if not self then return end
		if obj.transform.root == self.VMS.lastVMSRoot then return self.VMS.lastVMS end -- return last VMS if we hit the same object
		local bc = obj.transform.root:Find("BodyContainer")
		-- for other vehicles the root becomes 'NetGameObject'
		if not bc then
			bc = obj.transform.root:Find("Assembly"):Find("BodyContainer")
		end
		if not bc then return end
		local VMS = bc.transform:Find("ClickableControls2VMS")
		if not VMS then return end -- no VMS present on this vehicle, probably not clickable
		VMS = VMS:GetComponent("LuaComponent")
		if not VMS.self.VMSID then return end
		self.VMS.lastVMSRoot = obj.transform.root
		self.VMS.lastVMS = VMS
		return VMS
	end,
	Update = function(self)
		local self = ClickableControls2
		if #self.VMS.VMSqueue < 1 then return end

		-- iterate queue backward so that entries may be removed while iterating
		for i=#self.VMS.VMSqueue, 1, -1 do
			local vehicleRoot = self.VMS.VMSqueue[i]

			if vehicleRoot and not Slua.IsNull(vehicleRoot) then
				-- HBATransferCreate component is deleted once spawning is complete
				if not vehicleRoot.gameObject:GetComponent("HBNetworking.HBATransferCreate") then
					-- finished spawning
					self.VMS.InitVehicle(vehicleRoot)
					table.remove(self.VMS.VMSqueue, i)
				end
			else
				table.remove(self.VMS.VMSqueue, i) -- invalid entry, cleanup
			end
		end
	end,
	VMSqueue = {},
	RegisterForVMS = function(vehicleRoot) -- registers a vehicle to the queue, to initialise VMS once vehicle is fully spawned
		local self = ClickableControls2
		if not vehicleRoot then return end
		-- only check if finished async spawning if not spawned by player
		if not vehicleRoot.isMyVehicle then
			-- HBATransferCreate component is deleted once spawning is complete
			if vehicleRoot.gameObject:GetComponent("HBNetworking.HBATransferCreate") then
				-- not finished spawning
				self.VMS.VMSqueue[#self.VMS.VMSqueue+1] = vehicleRoot
				return
			end
		end
		-- finished spawning, safe to initialise
		self.VMS.InitVehicle(vehicleRoot)
	end,
	InitVehicle = function(vehicleRoot) -- add a VMS to a vehicle that spawned
		local self = ClickableControls2
		if not vehicleRoot then return end
		-- add component to vehicle
		local bodyContainer = vehicleRoot.gameObject:GetComponentInChildren("UnityEngine.Rigidbody").gameObject
		if not bodyContainer then return end
		local VMSobj = GameObject("ClickableControls2VMS")
		VMSobj.transform.parent = bodyContainer.gameObject.transform
		VMSobj.transform.localPosition = Vector3.zero
		VMSobj.transform.localRotation = Quaternion.identity
		local nb = VMSobj:AddComponent("HBNetworking.NetworkBase")
		local g = VMSobj:AddComponent("HBNetworking.General")
		if not HBU.FileExists(HBU.GetModLuaFolder() .. "/ClickableControls2/Scripts/ClickableControls2VMS.lua") then return end
		local CC2VMS = HBU.AddComponentLua(VMSobj.gameObject,"ModLua/ClickableControls2/Scripts/ClickableControls2VMS.lua")
		CC2VMS.self.VMSComponent = CC2VMS
		CC2VMS.self.created = Time.time
		CC2VMS.self.vehicleRoot = vehicleRoot
		CC2VMS.self.VMSID = self.IDManager:GetNewID()
		CC2VMS.self.networkBase = nb
		CC2VMS.self.generalBase = g
		CC2VMS.self:Init()
	end
}

-- find new vehicles in scene
ClickableControls2.FindNewObjects = {
	updateEveryXFrames = 10,
	AllVehicles = {},
	NewVehicles = {},
	newVehiclesThisFrame = false,
	removedVehiclesThisFrame = false,
	changesThisFrame = false,
	Update = function(self)
		return select(2,pcall(function(...)
				self.tick = ( self.tick or -1 ) + 1
				self.newVehiclesThisFrame = false
				self.removedVehiclesThisFrame = false
				self.changesThisFrame = false
				while next(self.NewVehicles) do self.NewVehicles[next(self.NewVehicles)] = nil; end
				self:Cleanup()
				if not self.VehicleManager then
					self.VehicleManager = Slua.GetClass("VehicleManager") or false
					if not self.VehicleManager then
						return
					end
				end
				if self.tick % ( self.updateEveryXFrames > 0 and self.updateEveryXFrames or 30 ) ~= 0 then
					return self.NewVehicles,self.NewPlayers
				end
				for v in Slua.iter(self.VehicleManager.allVehicles) do
					local vStr = tostring(v:GetInstanceID())
					if not self.AllVehicles[vStr] then
						self.NewVehicles[#self.NewVehicles+1] = v
						self.AllVehicles[vStr] = v
						self.newVehiclesThisFrame = true
						self.changesThisFrame = true
					end
				end
				return self.NewVehicles
			  end
			)
		)
	end,

	Cleanup = function(self)
		if not self.CleanupTables then
			self.CleanupTables = {self.AllVehicles}
		end
		for kt,t in pairs(self.CleanupTables) do
			for k,v in pairs(t) do
				if not v or Slua.IsNull(v) then
					t[k] = nil
					if kt == 1 then
						self.removedVehiclesThisFrame  = true
						self.changesThisFrame = true
					end
				end
			end
		end
    end,
}

function ClickableControls2.Lerp(a,b,t)
	return a * (1-t) + b * t
end

function ClickableControls2.InverseLerp(from, to, value)
	if from < to then
		if value < from then return 0 end
		if value > to then return 1 end
		return (value - from)/(to - from)
	end
	if from <= to or value > from then return 0 end
	if value < to then return 1 end
	return 1.0 - ((value - to) / (from - to))
end

-- what are you looking for down here? that's all there is, go home.

return ClickableControls2 -- fin
