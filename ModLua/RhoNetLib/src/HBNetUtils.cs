﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using SLua;
using HBNetworking;

// yes i know this code is ugly, it was hastily written and making things just *work* in SLua is annoying.

namespace RhoNetLib
{
	public class HBNetUtils
	{
		public byte[] CreateByteArray(int length)
		{
			return new byte[length];
		}
		public void ConvertToBytes(byte[] byteArray, object data, string dataType, int startIndex)
		{
			switch (dataType)
			{
				case "bool":
					Buffer.BlockCopy(BitConverter.GetBytes(Convert.ToBoolean(data)), 0, byteArray, startIndex, 1);
					break;
				case "short":
					Buffer.BlockCopy(BitConverter.GetBytes(Convert.ToInt16(data)), 0, byteArray, startIndex, 2);
					break;
				case "ushort":
					Buffer.BlockCopy(BitConverter.GetBytes(Convert.ToUInt16(data)), 0, byteArray, startIndex, 2);
					break;
				case "int":
					Buffer.BlockCopy(BitConverter.GetBytes(Convert.ToInt32(data)), 0, byteArray, startIndex, 4);
					break;
				case "uint":
					Buffer.BlockCopy(BitConverter.GetBytes(Convert.ToUInt32(data)), 0, byteArray, startIndex, 4);
					break;
				case "long":
					Buffer.BlockCopy(BitConverter.GetBytes(Convert.ToInt64(data)), 0, byteArray, startIndex, 8);
					break;
				case "ulong":
					Buffer.BlockCopy(BitConverter.GetBytes(Convert.ToUInt64(data)), 0, byteArray, startIndex, 8);
					break;
				case "float":
					Buffer.BlockCopy(BitConverter.GetBytes(Convert.ToSingle(data)), 0, byteArray, startIndex, 4);
					break;
				case "double":
					Buffer.BlockCopy(BitConverter.GetBytes(Convert.ToDouble(data)), 0, byteArray, startIndex, 8);
					break;
			}
		}
		public object ConvertFromBytes(byte[] data, string dataType, int startIndex)
		{

			switch (dataType)
			{
				case "bool":
					return BitConverter.ToBoolean(data, startIndex);
				case "short":
					return BitConverter.ToInt16(data, startIndex);
				case "ushort":
					return BitConverter.ToUInt16(data, startIndex);
				case "int":
					return BitConverter.ToInt32(data, startIndex);
				case "uint":
					return BitConverter.ToUInt32(data, startIndex);
				case "long":
					return BitConverter.ToInt64(data, startIndex);
				case "ulong":
					return BitConverter.ToUInt64(data, startIndex);
				case "float":
					return BitConverter.ToSingle(data, startIndex);
				case "double":
					return BitConverter.ToDouble(data, startIndex);
			}
			return null;
		}

		public Action ToDelegate(LuaFunction func)
		{
			return new Action(delegate ()
			{
				func.call();
			});
		}

		public Action<int> ToIntDelegate(LuaFunction func)
		{
			return new Action<int>(delegate (int generalIn)
			{
				func.call(generalIn);
			});
		}
		public Action<GeneralData> ToGeneralDataDelegate(LuaFunction func)
		{
			return new Action<GeneralData>(delegate (GeneralData generalIn)
			{
				func.call(generalIn);
			});
		}

		public Action<ServerData> ToServerDataDelegate(LuaFunction func)
		{
			return new Action<ServerData>(delegate (ServerData generalIn)
			{
				func.call(generalIn);
			});
		}
		public Action<AssetRefrence> ToAssetRefrenceDelegate(LuaFunction func)
		{
			return new Action<AssetRefrence>(delegate (AssetRefrence generalIn)
			{
				func.call(generalIn);
			});
		}
		public Action<TransferData> ToTransferDataDelegate(LuaFunction func)
		{
			return new Action<TransferData>(delegate (TransferData generalIn)
			{
				func.call(generalIn);
			});
		}
		public Action<PingData> ToPingDataDelegate(LuaFunction func)
		{
			return new Action<PingData>(delegate (PingData generalIn)
			{
				func.call(generalIn);
			});
		}
		public Action<PositionData> ToPositionDataDelegate(LuaFunction func)
		{
			return new Action<PositionData>(delegate (PositionData generalIn)
			{
				func.call(generalIn);
			});
		}
		public Action<AddComponentData> ToAddComponentDataDelegate(LuaFunction func)
		{
			return new Action<AddComponentData>(delegate (AddComponentData generalIn)
			{
				func.call(generalIn);
			});
		}
		public Action<ChatMessage> ToChatMessageDelegate(LuaFunction func)
		{
			return new Action<ChatMessage>(delegate (ChatMessage generalIn)
			{
				func.call(generalIn);
			});
		}


		public void RegisterCall(NetworkBase nb, NetworkCallType type, Action del)
		{
			nb.RegisterCall(type, del);
		}

		public void RegisterChatMessageCall(NetworkBase nb, NetworkCallType type, Action<ChatMessage> del)
		{
			nb.RegisterCall(type, del);
		}

		public void RegisterGeneralDataCall(NetworkBase nb, NetworkCallType type, Action<GeneralData> del)
		{
			nb.RegisterCall(type, del);
		}
		public void RegisterPositionDataCall(NetworkBase nb, NetworkCallType type, Action<PositionData> del)
		{
			nb.RegisterCall(type, del);
		}

		public void RegisterIntCall(NetworkBase nb, NetworkCallType type, Action<int> del)
		{
			nb.RegisterCall(type, del);
		}
	}
}
