-- By RhoSigma

BuilderToolLoader={}

BuilderToolLoader.version = "1.7 01/01/2020 BETA"

BuilderToolLoader.modPartDirs = {}

BuilderToolLoader.myPartsRoot = HBU.GetUserDataPath().."/MyParts/"

BuilderToolLoader.hbChat = GameObject.FindObjectOfType("HBChat")

BuilderToolLoader.ScriptComponents = {}

BuilderToolLoader.Refresh = function(self)
	if not self then return end
	if not self.rootPaths then return end
	if not self.currentPath then self.currentPath = "" end
	if not self.bar then return end
	self.bar:ClearButtons()

	if not self.currentPath or self.currentPath == "" and self.rootPaths and type(self.rootPaths) == "table" and #self.rootPaths > 1 then
		--if self.rootPaths and type(self.rootPaths) == "table" and #self.rootPaths > 1 then

		for i,dir in ipairs(self.rootPaths) do
			local maskName = HBU.GetFileNameWithoutExtension(dir)
			if self.rootMaskNames and self.rootMaskNames[i] then maskName = self.rootMaskNames[i] end
			local buttonConfig = self.bar:GetDefaultButtonConfig()
			buttonConfig.name = maskName
			buttonConfig.func = function() self:OpenDirectory(dir) end
			buttonConfig.icon = "lua/icons/builder/folder.png"
			buttonConfig.tooltip = maskName
			buttonConfig.hoversound = "click_blip"
			buttonConfig.clicksound = "affirm_blip_1"
			buttonConfig.imageLayout = {"dualcolor",true,"size",(self.gridSize or Vector2(self.barHeight,self.barHeight))*0.7}
	        buttonConfig.layout = {--[["colors",colorblock,]]"min",Vector2(120,0),"onenter",function() if self.onHoverDirectory then self.onHoverDirectory(buttonConfig,dir) end end}
			buttonConfig.showTextAndImage = true

			-- get any value changes from xmlFile
			local path = HBU.GetDirectoryName(dir) .. "/" .. HBU.GetFileNameWithoutExtension(dir) .. "_bui.hbui"
			if HBU.FileExists(path) then
				buttonConfig = BuilderToolLoader.XmlToTable(path, buttonConfig)
				buttonConfig.icon = HBU.GetDirectoryName(dir) .. "/" .. buttonConfig.icon
			end

			self.bar:AddButton(buttonConfig)
		end

		--end
	else
		if not self.currentPath or self.currentPath == "" and self.rootPaths and type(self.rootPaths) == "table" and #self.rootPaths == 1 then
			self.currentPath = self.rootPaths[1]
		end

		local files = {}
		local dirs = {}
		-- if we're in the stock myparts folder, load in mod myparts folders and parts
		if self.currentPath == HBU.GetUserDataPath().."/MyParts" then
			files = iter(HBU.GetFiles(self.currentPath,self.extension))
			dirs = iter(HBU.GetDirectories(self.currentPath,"*"))
			for k,modDir in ipairs(BuilderToolLoader.modPartDirs) do
				for modPartDir in Slua.iter(HBU.GetDirectories(modDir,"*")) do
					table.insert(dirs, modPartDir)
				end
				for modPartPath in Slua.iter(HBU.GetFiles(modDir,self.extension)) do
					table.insert(files, modPartPath)
				end
			end
		else
			files = iter(HBU.GetFiles(self.currentPath,self.extension))
			dirs = iter(HBU.GetDirectories(self.currentPath,"*"))
		end
		self.paths = dirs
		self.files = files

		for y,dir in ipairs(dirs) do
			local buttonConfig = self.bar:GetDefaultButtonConfig()
			buttonConfig.name = HBU.GetFileNameWithoutExtension(dir)
			buttonConfig.func = function() self:OpenDirectory(dir) end
			buttonConfig.icon = "lua/icons/builder/folder.png"
			buttonConfig.tooltip = HBU.GetFileNameWithoutExtension(dir)
			buttonConfig.hoversound = "click_blip"
			buttonConfig.clicksound = "affirm_blip_1"
			buttonConfig.imageLayout = {"dualcolor",true,"size",(self.gridSize or Vector2(self.barHeight,self.barHeight))*0.7}
	        buttonConfig.layout = {--[["colors",colorblock,]]"min",Vector2(120,0),"onenter",function() if self.onHoverDirectory then self.onHoverDirectory(buttonConfig,dir) end end}
			buttonConfig.showTextAndImage = true

			-- get any value changes from xmlFile
			local path = HBU.GetDirectoryName(dir) .. "/" .. HBU.GetFileNameWithoutExtension(dir) .. "_bui.hbui"
			if HBU.FileExists(path) then
				buttonConfig = BuilderToolLoader.XmlToTable(path, buttonConfig)
				buttonConfig.icon = HBU.GetDirectoryName(dir) .. "/" .. buttonConfig.icon
				Debug.Log(buttonConfig.layout.colors)
				buttonConfig.layout.colors = ColorBlock.defaultColorBlock
			end

			self.bar:AddButton(buttonConfig)
		end

		for x,filename in ipairs(files) do
			local buttonConfig = self.bar:GetDefaultButtonConfig()
			buttonConfig.name = HBU.GetFileNameWithoutExtension(filename)
			buttonConfig.func = function() if self.onClickFile then self.onClickFile(buttonConfig,filename) end end
			buttonConfig.image = BUI.LoadImageThroughCache(filename)
			buttonConfig.imageLayout = {"size",(self.gridSize or Vector2(self.barHeight,self.barHeight))*0.9}
			buttonConfig.tooltip = HBU.GetFileNameWithoutExtension(filename)
			buttonConfig.hoversound = "click_blip"
			buttonConfig.clicksound = "affirm_blip_1"
			buttonConfig.layout = {--[["colors",colorblock,]]"onenter",function() if self.onHoverFile then self.onHoverFile(buttonConfig,filename) end end}
			buttonConfig.showTextAndImage = self.showTextAndImage or false
			self.bar:AddButton(buttonConfig)
		end
	end
	self.bar:SetupBarDrag()
end

BuilderToolLoader.Search = function(self)
	if not self then return end
	if not self.search or not type(self.search) == "string" or self.search == "" then return end
	local results = {}
	if self.rootPaths then
		for i,rootPath in ipairs(self.rootPaths) do
			table.merge(results,iter(HBU.SearchFiles(rootPath,self.search,"*hbm",true)))
		end
		for i,modPath in ipairs(BuilderToolLoader.modPartDirs) do
			table.merge(results,iter(HBU.SearchFiles(modPath,self.search,"*hbm",true)))
		end
	end

	self.bar:ClearButtons()

	for x,filename in ipairs(results) do
		local buttonConfig = self.bar:GetDefaultButtonConfig()
		buttonConfig.name = HBU.GetFileNameWithoutExtension(filename)
		buttonConfig.func = function() if self.onClickFile then self.onClickFile(buttonConfig,filename) end end
		buttonConfig.image = BUI.LoadImageThroughCache(filename)
		buttonConfig.imageLayout = {"size",(self.gridSize or Vector2(self.barHeight,self.barHeight))*0.9}
		buttonConfig.tooltip = HBU.GetFileNameWithoutExtension(filename)
		buttonConfig.hoversound = "click_blip"
		buttonConfig.clicksound = "affirm_blip_1"
		buttonConfig.layout = {--[["colors",colorblock,]]"onenter",function() if self.onHoverFile then self.onHoverFile(buttonConfig,filename) end end}
		buttonConfig.showTextAndImage = self.showTextAndImage or false
		self.bar:AddButton(buttonConfig)
	end

	self.backButton:SetActive(true)
	self.currentPath = "search"

	if self.onSearch then self.onSearch(self.search) end
end

BuilderToolLoader.Back = function(self)
	if not self then return end
	if not self.currentPath or self.currentPath == "" then return end
	--go back to root if path = "search"
	if self.currentPath == "search" then
		self.currentPath = ""
		self.backButton:SetActive(false)
		if self.onBack then self.onBack(self.currentPath) end
			self:Refresh();
		return
	end

	-- if in a myparts folder but parent dir isn't the default myparts folder, swap over to stock myparts
	local parentDir = string.lower(HBU.GetDirectoryName(self.currentPath))
	if HBU.GetFileName(parentDir) == "myparts" then
		if parentDir ~= string.lower(HBU.GetUserDataPath().."/MyParts") then
			print("returned to stock myparts from mod myparts")
			self.currentPath = HBU.GetUserDataPath().."/MyParts"
			self:Refresh();
			return
		end
	end

	--if currently on any root path dont go back futher
	if self.rootPaths then
		for i,rootPath in ipairs(self.rootPaths) do
			if string.lower(HBU.GetDirectoryName(self.currentPath)) == string.lower(HBU.GetDirectoryName(rootPath)) then
				self.currentPath = "";
				self.backButton:SetActive(false)
				if self.onBack then self.onBack(self.currentPath) end
					self:Refresh();
				return
			end
		end
	end

	--go one folder bac
	self.currentPath = HBU.GetDirectoryParent(self.currentPath)
	if self.onBack then self.onBack(self.currentPath) end
	self:Refresh()
end

BUI.BrowserBar.Refresh = BuilderToolLoader.Refresh
BUI.BrowserBar.Back = BuilderToolLoader.Back
BUI.BrowserBar.Search = BuilderToolLoader.Search

BuilderToolLoader.Copy = function(self)
	if not self then return end
	--cache current selection
	BuilderToolLoader:ExecuteCallback("copy")
	self.copyGameObjects = iter(HBBuilder.Builder.selection)
	Builder.Play("copy")--Audio:Play("affirm-melodic-4",true,true);
end

function BuilderToolLoader:BuilderOpen()
	SelectTool.Copy = BuilderToolLoader.Copy
end

function BuilderToolLoader:Awake()
	Debug.Log("Loaded ToolLoader")
	BuilderToolLoader.partsFolder = HBU.GetUserDataPath().."/MyParts"
	self:AddCommands()
	HBBuilder.Builder.RegisterCallback(HBBuilder.BuilderCallback.BuilderLoaded, "BTL onBuilderLoaded", function() BuilderToolLoader.BuilderOpen() end)
	BuilderToolLoader:FindTools(HBU.GetBuilderLuaFolder() .. "/Vehicle")
end

-- very basic, json is better anyway.
BuilderToolLoader.XmlToTable = function(path, inTable)
	if not path then return inTable end
	if not HBU.FileExists(path) then return inTable end
	local xmlFile = HBU.XmlLoad(path)
	for k, v in pairs(inTable) do
		local elements = HBU.XmlElements(xmlFile, k)
		if elements.Length == 1 then
			inTable[k] = HBU.XmlValue(elements[1])
		end
	end
	return inTable
end

function BuilderToolLoader:CheckModBTLVersion()
	local versionStringToNumber = function(versionString)
		local returnVersion = tonumber(string.match(versionString, '^.- '))
		if returnVersion == nil then returnVersion = tonumber(versionString) end
		return returnVersion
	end
	local BTLversionNumber = versionStringToNumber(BuilderToolLoader.version)
	for k,script in ipairs(BuilderToolLoader.builderScripts) do
		local luaTable = script.self
		if luaTable.requiredBTLversion then
			local requiredBTLBersion = luaTable.requiredBTLversion
			if type(requiredBTLBersion) == "string" then requiredBTLBersion = versionStringToNumber(requiredBTLBersion) end
			if requiredBTLBersion > BTLversionNumber then
				BuilderToolLoader.hbChat:AddMessage("[BuilderTooLoader]", tostring(script.name) .. " requires a later version of BuilderToolLoader!")
				BuilderToolLoader.hbChat:AddMessage("[BuilderTooLoader]", "Current BTL version: " .. tostring(BTLversionNumber))
				BuilderToolLoader.hbChat:AddMessage("[BuilderTooLoader]", tostring(script.name) .. " requires: " .. tostring(requiredBTLBersion))
			end
		end
	end
end

function BuilderToolLoader:AddCommands()
	GConsole.AddCommand("myparts", "Opens the MyParts folder" , function() Application.OpenURL(BuilderToolLoader.partsFolder) end)
end

BuilderToolLoader.callbacks = {
["copy"] = {},
["paste"] = {},
}

function BuilderToolLoader:ExecuteCallback(callbackType)
	if not callbackType then return end
	if not BuilderToolLoader.callbacks[callbackType] then return end
	if #BuilderToolLoader.callbacks[callbackType] == 0 then return end
	for i,callbackFunc in ipairs(BuilderToolLoader.callbacks[callbackType]) do
		if pcall(callbackFunc) then
		else
			Debug.Log("BuilderToolLoader callback failed!")
		end
	end
end


function BuilderToolLoader:AddCallback(callbackType, callbackFunc)
	if not callbackType or not type(callbackType) == "string" then return false end
	if not callbackFunc or not type(callbackFunc) == "function" then return false end
	if not BuilderToolLoader.callbacks[string.lower(callbackType)] then return false end
	table.insert(BuilderToolLoader.callbacks[string.lower(callbackType)], callbackFunc)
	return true
end

function BuilderToolLoader:FindTools(dir)
	Debug.Log("FindTools")
	for luaFile in Slua.iter(HBU.GetFiles(HBU.GetBuilderLuaFolder() .. "/Vehicle", "*.lua")) do
		Debug.Log(luaFile)
		self:AddResources(luaFile)
	end
end

function BuilderToolLoader:AddResources(luaFile)
	if not luaFile then return end
	local rscRoot = HBU.GetBuilderLuaFolder() .. "/Vehicle/" .. tostring(HBU.GetFileNameWithoutExtension(luaFile))
	if HBU.DirectoryExists(rscRoot) then
		Debug.Log("BuilderToolLoader:	Loading resources for:	" .. tostring(tool))
		if HBU.DirectoryExists(rscRoot .. "/MyParts") then
			table.insert(BuilderToolLoader.modPartDirs,rscRoot .. "/MyParts")
		end
	end
end

function BuilderToolLoader:OnDestroy()
	GConsole.RemoveCommand("myparts")
end

return BuilderToolLoader
