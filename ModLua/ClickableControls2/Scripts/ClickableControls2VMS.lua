-- ClickableControls2 Vehicle Manager Script
-- By RhoSigma

ClickableControls2VMS = {}
ClickableControls2VMS.version = "1.0 6/04/2020"

ClickableControls2VMS.VMSComponent = nil
ClickableControls2VMS.VMSID = ""
ClickableControls2VMS.vehicleRoot = nil
ClickableControls2VMS.created = 0
ClickableControls2VMS.lastSynced = 0


function ClickableControls2VMS:Awake() -- VMS not init yet on first awake call.
	Debug.Log("VMS AWAKE")
end

ClickableControls2VMS.sw = Slua.CreateClass("System.Diagnostics.Stopwatch") --ClickableControls2VMS.sw.Reset();ClickableControls2VMS.sw.Start()		ClickableControls2VMS.sw.Stop();Debug.Log(ClickableControls2VMS.sw.Elapsed)


function ClickableControls2VMS:Init()
	ClickableControls2VMS.sw.Reset();ClickableControls2VMS.sw.Start()
	if not self.vehicleRoot then return end

	-- all networkBases attached to a Rigidbody
	self.ownerBases = {}

	self.clickables = {}
	-- get all clickables
	for button in Slua.iter(self.vehicleRoot:GetComponentsInChildren("HBButton")) do
		local sDataComp = button.gameObject:GetComponent("HBBuilder.StringDataContainer")
		if sDataComp then
			if HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, "clickableControl") == "true" then
				table.insert(self.clickables,button)
			end
		end
	end
	if #self.clickables > 0 then
		ClickableControls2.VMS.RegisterVMS({self.VMSID, self.VMSComponent})
		for i,clickable in ipairs(self.clickables) do
			if clickable then
				local hitbox = clickable.gameObject.transform:Find("ClickableHitbox").gameObject
				if hitbox then
					if hitbox:GetComponent(BoxCollider) then -- clean up BoxCollider if it exists
						GameObject.Destroy(hitbox:GetComponent(BoxCollider))
					end
					if hitbox:GetComponent("HBBuilder.StringDataContainer") then
						local sDataComp = hitbox:GetComponent("HBBuilder.StringDataContainer")
						local coll =  hitbox:AddComponent(BoxCollider)
						if coll then

							-- set collider
							local collSize = Vector3(tonumber(HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, "boxX")),tonumber(HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, "boxY")),tonumber(HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, "boxZ")))
							coll.size = collSize
							coll.center = Vector3.zero
							coll.isTrigger = true
							coll.gameObject.layer = 6

							-- restructure clickables table
							local sDataComp = clickable.gameObject:GetComponent("HBBuilder.StringDataContainer")

							self.clickables[i] = {
								["button"] = clickable,
								["hitbox"] = coll,
								["params"] = {},
								["moveables"] = {},
								["info"] = {},
								["tags"] = {},
								["tagCount"] = 0,
							}

							-- networking
							if ClickableControls2.Networking.isConnected then
								local nb = clickable.gameObject:GetComponent("HBNetworking.NetworkBase")
								local g = clickable.gameObject:GetComponent("HBNetworking.General")
								if nb and g then
									-- on general data recieved callback delegate
									local OnGeneral = function(generalData) ClickableControls2.Networking.RecieveSync(self.clickables[i], generalData) end
									local delegate = RhoNetLib.HBNetworking.ToDelegate(OnGeneral, "GeneralData")
									RhoNetLib.HBNetworking.HBNetUtils:RegisterGeneralDataCall(nb, RhoNetLib.HBNetworking.NetworkCallType.General, delegate)
									self.clickables[i].networking = {}
									self.clickables[i].networking.nb = nb
									self.clickables[i].networking.g = g
									self.clickables[i].networking.ownerBase = clickable.gameObject:GetComponentInParent("UnityEngine.Rigidbody").gameObject:GetComponent("HBNetworking.NetworkBase")

									-- add to ownerBases list and add general callback
									if not self.ownerBases[self.clickables[i].networking.ownerBase] then
										self.ownerBases[self.clickables[i].networking.ownerBase] = true
										local OnGeneral = function(generalData) ClickableControls2.Networking.RecieveOwner(self.clickables[i].networking.ownerBase, generalData) end
										local delegate = RhoNetLib.HBNetworking.ToDelegate(OnGeneral, "GeneralData")
										RhoNetLib.HBNetworking.HBNetUtils:RegisterGeneralDataCall(self.clickables[i].networking.ownerBase, RhoNetLib.HBNetworking.NetworkCallType.General, delegate)
									end

									self.clickables[i].networking.syncMasterConnID = -1
									if self.clickables[i].networking.ownerBase.owner then
										self.clickables[i].networking.syncMasterConnID = ClickableControls2.Networking.NetworkManager.connectors[0].myConnID
									end
									self.clickables[i].networking.lastSyncTime = 0
								end
							end

							if sDataComp then
								for key in Slua.iter(HBBuilder.BuilderUtils.GetStringDataKeys(sDataComp.stringData)) do
									self.clickables[i].params[key] = HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, key)
								end
							end

							for moveable in Slua.iter(clickable.moveables) do
								local sDataComp = moveable.gameObject:GetComponent("HBBuilder.StringDataContainer")
								local moverParams = {}
								moverParams["moveable"] = moveable
								if sDataComp then
									for key in Slua.iter(HBBuilder.BuilderUtils.GetStringDataKeys(sDataComp.stringData)) do
										moverParams[key] = HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, key)
									end
								end
								table.insert(self.clickables[i].moveables, moverParams)
							end
							if clickable.gameObject.transform:Find("ClickableInfo") then
								local sDataComp = clickable.gameObject.transform:Find("ClickableInfo").gameObject:GetComponent("HBBuilder.StringDataContainer")
								if sDataComp then
									for key in Slua.iter(HBBuilder.BuilderUtils.GetStringDataKeys(sDataComp.stringData)) do
										self.clickables[i].info[key] = HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, key)
									end
								end
							end
							if clickable.gameObject.transform:Find("StateTags") then
								local sDataComp = clickable.gameObject.transform:Find("StateTags").gameObject:GetComponent("HBBuilder.StringDataContainer")
								if sDataComp then
									for key in Slua.iter(HBBuilder.BuilderUtils.GetStringDataKeys(sDataComp.stringData)) do
										local tag = tostring(HBBuilder.BuilderUtils.GetStringData(sDataComp.stringData, key))
										if tag then
											if tag ~= "" then
												self.clickables[i].tags[tag] = true
												self.clickables[i].tagCount = self.clickables[i].tagCount + 1
											end
										end
									end
								end
							end
						end
					end
				end

				-- clikable was created

				-- run first time init on clickable
				if ClickableControls2.Clickables.clickableTypes[self.clickables[i].params.type] then
					if ClickableControls2.Clickables.clickableTypes[self.clickables[i].params.type].init then
						ClickableControls2.Clickables.clickableTypes[self.clickables[i].params.type].init(self.clickables[i])
					end
				end

				ClickableControls2.Clickables.UpdateMovables(self.clickables[i])
				self.clickables[i].button:UpdateOutputs()
			end
		end
	else -- no buttons on vehicle, destroy VMS
		GameObject.Destroy(self.VMSComponent)
		GameObject.Destroy(self.VMSComponent.gameObject)
		return
	end

	-- add ownerSwitch callbacks to vehiclePieces
	-- if ClickableControls2.Networking.isConnected then
	--
	-- end
	self:UpdateClickableStatus()
	ClickableControls2VMS.sw.Stop();Debug.Log("CC2VMS finished first time init in: " .. tostring(ClickableControls2VMS.sw.Elapsed))
end

function ClickableControls2VMS:UpdateClickableStatus() -- only update if rb is not kinematic, if rb in parent of clickable kinematic disable all clickable colliders
	for i=1,#self.clickables do
		local clickable = self.clickables[i]
		local enabled = true -- should clickable collider be enabled?
		local owner = true

		if ClickableControls2.Networking.isConnected then
			owner = clickable.networking.ownerBase.owner
			enabled = owner
		end

		if clickable.tagCount > 0 then -- don't check tags if clickable has none
			enabled = false -- set to false, if all checks fail, disable clickable

			if clickable.tags["otherPlayer"] or owner then
				for tag,v in pairs(clickable.tags) do -- iterate clickable tags, if tag is matched to player state, enable clickable
					if ClickableControls2.Interaction.playerStatus[tag] then
						-- if tag match found, skip other checks, enable clickable
						enabled = true
						break
					end
				end
			end
		end
		clickable.hitbox.enabled = enabled
	end
end

function ClickableControls2VMS:OnDestroy()
	Debug.Log("destroyed VMS")
	ClickableControls2.VMS.UnRegisterVMS(self.VMSID)
end


return ClickableControls2VMS
