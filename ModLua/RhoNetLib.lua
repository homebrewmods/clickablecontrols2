RhoNetLib = {}
-- By RhoSigma

RhoNetLib.version = "1.0 6/04/2020"

function RhoNetLib:Awake()
	self.HBNetworking.Init()
end


RhoNetLib.HBNetworking = {
	-- dllPath = "F:/Homebrew/Modding/Mods/RhoNetLib/RhoNetLib/RhoNetLib/bin/Debug/RhoNetLib.dll",
	dllPath = HBU.GetModLuaFolder() .. "/RhoNetLib/RhoNetLib.dll",
	NetworkCallTypeClass = Slua.GetClass("HBNetworking.NetworkCallType"),
	Init = function()
		local self = RhoNetLib
		Slua.GetClass("System.Reflection.Assembly").LoadFrom(self.HBNetworking.dllPath)
		self.HBNetworking.HBNetUtils = Slua.CreateClass("RhoNetLib.HBNetUtils")

		self.HBNetworking.NetworkCallType = {
			Start = self.HBNetworking.NetworkCallTypeClass.Start,
			Update = self.HBNetworking.NetworkCallTypeClass.Update,
			Connect = self.HBNetworking.NetworkCallTypeClass.Connect,
			Connecting = self.HBNetworking.NetworkCallTypeClass.Connecting,
			Disconnect = self.HBNetworking.NetworkCallTypeClass.Disconnect,
			ConnectFail = self.HBNetworking.NetworkCallTypeClass.ConnectFail,
			ForceReconnect = self.HBNetworking.NetworkCallTypeClass.ForceReconnect,
			OtherJoined = self.HBNetworking.NetworkCallTypeClass.OtherJoined,
			OtherLeft = self.HBNetworking.NetworkCallTypeClass.OtherLeft,
			OwnerSwitch = self.HBNetworking.NetworkCallTypeClass.OwnerSwitch,
			AskStateSync = self.HBNetworking.NetworkCallTypeClass.AskStateSync,
			StateSync = self.HBNetworking.NetworkCallTypeClass.StateSync,
			AddComponent = self.HBNetworking.NetworkCallTypeClass.AddComponent,
			General = self.HBNetworking.NetworkCallTypeClass.General,
			Position = self.HBNetworking.NetworkCallTypeClass.Position,
			Ping = self.HBNetworking.NetworkCallTypeClass.Ping,
			Remove = self.HBNetworking.NetworkCallTypeClass.Remove,
			Transfered = self.HBNetworking.NetworkCallTypeClass.Transfered,
			Transfering = self.HBNetworking.NetworkCallTypeClass.Transfering,
			TransferProgress = self.HBNetworking.NetworkCallTypeClass.TransferProgress,
			Created = self.HBNetworking.NetworkCallTypeClass.Created,
			ApplyServerSettings = self.HBNetworking.NetworkCallTypeClass.ApplyServerSettings,
			AdminLogin = self.HBNetworking.NetworkCallTypeClass.AdminLogin,
			ServerInitializeData = self.HBNetworking.NetworkCallTypeClass.ServerInitializeData,
			ChatMessage = self.HBNetworking.NetworkCallTypeClass.ChatMessage,
			NetworkTimeSync = self.HBNetworking.NetworkCallTypeClass.NetworkTimeSync,
		}
	end,

	ToDelegate = function(func, argType)
		local self = RhoNetLib
		if not func then return end
		local delegate = ""
		if not argType then
			delegate = self.HBNetworking.HBNetUtils:ToDelegate(func)
		elseif argType == "int" then
			delegate = self.HBNetworking.HBNetUtils:ToIntDelegate(func)
		elseif argType == "GeneralData" then
			delegate = self.HBNetworking.HBNetUtils:ToGeneralDataDelegate(func)
		elseif argType == "ServerData" then
			delegate = self.HBNetworking.HBNetUtils:ToServerDataDelegate(func)
		elseif argType == "AssetRefrence" then
			delegate = self.HBNetworking.HBNetUtils:ToAssetRefrenceDelegate(func)
		elseif argType == "TransferData" then
			delegate = self.HBNetworking.HBNetUtils:ToTransferDataDelegate(func)
		elseif argType == "PingData" then
			delegate = self.HBNetworking.HBNetUtils:ToPingDataDelegate(func)
		elseif argType == "PositionData" then
			delegate = self.HBNetworking.HBNetUtils:ToPositionDataDelegate(func)
		elseif argType == "AddComponentData" then
			delegate = self.HBNetworking.HBNetUtils:ToAddComponentDataDelegate(func)
		elseif argType == "ChatMessage" then
			delegate = self.HBNetworking.HBNetUtils:ToChatMessageDelegate(func)
		else
			error("RhoNetLib ToDelegate: Unknown delegate type")
			return
		end
		return delegate
	end,

	-- RegisterCall = function(networkBase, type, delegate)
	-- 	local self = RhoNetLib
	-- 	if not networkBase then return end
	-- 	if not type then return end
	-- 	if not delegate then return end
	-- 	if not self.HBNetworking.NetworkCallType[type] then error("RhoNetLib RegisterCall: Unknown NetworkCallType"); return end
	-- 	if type then
	-- 		self.HBNetworking.HBNetUtils:RegisterChatMessageCall(networkBase, self.HBNetworking.NetworkCallType[type], delegate)
	-- 	end
	--
	--
	-- end,

}

return RhoNetLib
